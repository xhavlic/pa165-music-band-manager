from locust import HttpUser, task
import random

class MapScenario(HttpUser):
    def on_start(self):
        self.token = "insert your token here"

    @task
    def getLocation(self):
        headers = {
            'Accept': 'Application/Json',
            'Authorization': 'Bearer ' + self.token
        }
        czech_cities = [
            "Prague", "Brno", "Ostrava", "Plzen", "Liberec", "Olomouc", "Usti nad Labem", "Ceske Budejovice",
        "Hradec Kralove", "Pardubice", "Zlin", "Havlickuv Brod", "Karlovy Vary", "Teplice", "Decin",
        "Jihlava", "Most", "Opava", "Frydek-Mistek", "Karlovy Vary", "Ceska Lipa", "Mlada Boleslav",
        "Trutnov", "Prerov", "Jablonec nad Nisou", "Prostejov", "Trebec", "Cheb", "Trinec", "Kolin"
        ]
        self.client.get("/api/map/" + random.choice(czech_cities), headers=headers)
