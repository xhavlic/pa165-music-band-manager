from locust import HttpUser, task
import random

class UserScenario(HttpUser):
    def on_start(self):
        self.token = "insert your token here"

    @task
    def getUsers(self):
        headers = {
            'Accept': 'Application/Json',
            'Authorization': 'Bearer ' + self.token
        }

        self.client.get("/api/users", headers=headers)
