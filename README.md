# PA165 Music Band Manager

## Music Band Manager
The web application allows one music band to manage their activities. Managers can create a new band and start hiring team members from a catalogue. They can set for the band the musical style (e.g. rock, pop, etc…), a name, and a logo. Band managers can add new albums and songs to a band. Each Song has a name and a duration. Managers can also schedule tours for the bands, in terms of dates and cities visited.
Team members can log in to the system and see the offers made by the band managers. They can accept or reject to be part of a band. After they are part of a band, they can see all the activities that are planned and the profiles of the other band members.
## Running with Podman Compose

To build and run the application with Podman Compose, follow these steps:

1. Install Podman and Podman Compose on your machine.
   - if it's your first time using Podman on current PC you need also run this commands :
      - `podman machine init`
      - `podman machine start`
2. Navigate to the root directory where `docker-compose.yml` is located.
3. Run `podman-compose build` to build the services.
4. Run `podman-compose up` to start the services.
   - [Band's Management Microservice](http://localhost:8090/swagger-ui/index.html) -> on port 8090  
   - [LoginService](http://localhost:8080) -> on port 8080
   - [User's Management Microservice](http://localhost:8082/swagger-ui/index.html) -> on port 8082
   - [Tour Management Microservice](http://localhost:8083/swagger-ui/index.html) -> on port 8083
   - [Prometheus](http://localhost:9090/) -> on port 9000
   - [Grafana](http://localhost:3000/) -> on port 3000

### How to log in
- To log in go to [LoginService](http://localhost:8080) log in with muni. Go to [/token](http://localhost:8080/token) to copy token needed for authentication.

### Seeding of Database
- To Seed the database with [faker](https://github.com/DiUS/java-faker) data, set for every microservice in `Dockerfile` the build variable `"-Dseed.data"` to `true` (default setting) 
  - otherwise If you don't want to seed database set it to `false` -> `"-Dseed.data=false"`
- To optionally clear the databases of microservices, set for every microservice in `Dockerfile` the build variable `"-Dseed.data"` to `true` (default setting) otherwise to `false`
  - please bear in mind that cleaning of database is done before Seeding. Which means if you set both to `true` database will be cleared and then seeded. 

## Description of microservices
### Band's Management Microservice
Handles all band-related functions, such as creating and updating band profiles, managing band member recruitment, album and song uploads, and overseeing incoming gig offers and other band-related activities.
#### Data Ownership:
- Band profiles (musical style, name, logo)
- Album and song information 
- Band member recruitment and roles within the band

### User's Management Microservice
Manages user profiles for all users aspirating to become members of the band, providing services for account creation, profile detail management, and authentication for band managers and team members.
#### Data Ownership:
- User profiles (personal information, preferences, credentials for login)
- Authentication and authorization data (passwords, roles, permissions)

### Tour Management Microservice
Facilitates the organization of tours, offering tools for viewing potential venues and scheduling tours for bands.
#### Data Ownership:
- Tour schedules (dates, cities)
- Potential venues and their details

## Scenario
The following scenario is for system response performance measuring.
By running the script located in `Locust/User` using a program called [Locust](https://locust.io/), we can see the performance of the system.

First, access the login service at `localhost:8080`. Login through identity provider (for example MUNI). Obtain an access token by accessing the `localhost:8080/token` after successful login.
Copy this token and paste into the `Locust/User/locustfile.py` and to a designated place marked 'insert your token here'.

Run using `locust` while in the directory with a script.
Access `localhost:8089` in your browser.

Here add host with `http://localhost:8083`, which represents the user service. Note that the login and the service must be running for this to work.

Now add number of peak users and ramp up and view the request response statistics.

Adding more users should increase the average response time, but shouldn't cause any failures until a really large number of users.



## User case diagram
![Use Case Diagram](misc/Use-Case.jpg?raw=true)

## Class diagram for the DTOs
![Dto's Class Diagram](misc/Dto's-Class-Diagram.jpg?raw=true)


