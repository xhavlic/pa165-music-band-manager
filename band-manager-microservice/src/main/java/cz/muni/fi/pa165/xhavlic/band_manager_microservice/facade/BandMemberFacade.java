package cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputBandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper.BandMemberMapper;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.service.BandMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BandMemberFacade {
    private final BandMemberService bandMemberService;

    private final BandMemberMapper bandMemberMapper;

    @Autowired
    public BandMemberFacade(BandMemberService bandMemberService,  BandMemberMapper bandMemberMapper) {
        this.bandMemberService = bandMemberService;
        this.bandMemberMapper = bandMemberMapper;
    }

    public BandMemberDto getBandMemberById(long id) {
        return bandMemberMapper.mapToDto(bandMemberService.getBandMemberById(id));
    }

    public BandMemberDto updateBandMember(long id, InputBandMemberDto bandMemberDto) {
        return bandMemberMapper.mapToDto(bandMemberService.updateBandMember(id, bandMemberMapper.mapFromDto(bandMemberDto)));
    }

    public BandMemberDto createNewBandMember(InputBandMemberDto bandMemberDto) {
        return bandMemberMapper.mapToDto(bandMemberService.createNewBandMember(bandMemberMapper.mapFromDto(bandMemberDto)));
    }

    public void deleteBandMember(long id) {
        bandMemberService.deleteBandMember(id);
    }

    public BandMemberDto addBandMemberToBand(long bandId, InputBandMemberDto bandMemberDto) {
        return bandMemberMapper.mapToDto(bandMemberService.addBandMemberToBand(bandId, bandMemberMapper.mapFromDto(bandMemberDto)));
    }

}
