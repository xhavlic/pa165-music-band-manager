package cz.muni.fi.pa165.xhavlic.band_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.DetailedBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade.BandFacade;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.oauth.BandServiceAuth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @author Jakub Uhlarik
 */

@RestController
@RequestMapping("/api/bands")
public class BandRestController {

    private final BandFacade bandFacade;

    @Autowired
    public BandRestController(BandFacade bandFacade) {
        this.bandFacade = bandFacade;
    }

    @Operation(summary = "Get a band by id", description = "Returns a band per the id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the band",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BandDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Band not found", content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<DetailedBandDto> getBandById(@PathVariable Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(bandFacade.getBandById(id));
    }


    @Operation(summary = "Add new band", description = "Add new band and returns band's id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Added the band",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BandDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid body supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content())
    })
    @PostMapping
    public ResponseEntity<BandDto> addNewBand(@RequestBody InputBandDto band) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bandFacade.addNewBand(band));
    }

    @Operation(summary = "Get all bands", description = "Get list of all bands")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the band",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = BandDto.class)))}),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content())})
    @GetMapping
    public ResponseEntity<List<BandDto>> listAllBands() {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(bandFacade.findAllBands());
    }

    @Operation(summary = "Update a band", description = "Update band with a id to band in body")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated band",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BandDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or body supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Band not found", content = @Content)})
    @PutMapping("{id}")
    public ResponseEntity<BandDto> updateBand(@PathVariable Long id, @RequestBody InputBandDto bandDto) {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(bandFacade.updateBand(id, bandDto));
    }

    @Operation(summary = "Delete a band", description = "Delete band with a id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_1"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the band"),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_1", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Band not found")})
    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteBand(@PathVariable long id) {
        bandFacade.deleteBand(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
