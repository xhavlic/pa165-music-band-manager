package cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.DetailedBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper.BandMapper;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.service.BandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jakub Uhlarik
 */

@Service
public class BandFacade {

    private final BandService bandService;
    private final BandMapper bandMapper;

    @Autowired
    public BandFacade(BandService bandService, BandMapper bandMapper) {
        this.bandService = bandService;
        this.bandMapper = bandMapper;
    }


    public DetailedBandDto getBandById(long id) {
        return bandMapper.mapToDetailedDto(bandService.getBandById(id));
    }

    public BandDto addNewBand(InputBandDto bandDto) {
        var band = bandMapper.mapFromDto(bandDto);
        return bandMapper.mapToDto(bandService.addNewBand(band));
    }

    public List<BandDto> findAllBands() {
        return bandMapper.mapToList(bandService.findAllBands());
    }

    public BandDto updateBand(long id, InputBandDto bandDto) {
        return bandMapper.mapToDto(bandService.updateBand(id, bandMapper.mapFromDto(bandDto)));
    }

    public void deleteBand(long id) {
        bandService.deleteBand(id);
    }
}
