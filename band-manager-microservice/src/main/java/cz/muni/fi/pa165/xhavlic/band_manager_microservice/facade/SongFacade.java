package cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputSongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.SongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper.SongMapper;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Jakub Uhlarik
 */
@Service
public class SongFacade {

    private final SongMapper songMapper;
    private final SongService songService;

    @Autowired
    public SongFacade(SongMapper songMapper, SongService songService) {
        this.songMapper = songMapper;
        this.songService = songService;
    }

    public SongDto addSongToAlbum(long albumId, InputSongDto songDto) {
        return songMapper.mapToDto(songService.addSongToAlbum(albumId, songMapper.mapFromDto(songDto)));
    }

    public SongDto getSongById(long id) {
        return songMapper.mapToDto(songService.getSongById(id));
    }

    public SongDto updateSong(long id, InputSongDto songDto) {
        return songMapper.mapToDto(songService.updateSong(id, songMapper.mapFromDto(songDto)));
    }
}
