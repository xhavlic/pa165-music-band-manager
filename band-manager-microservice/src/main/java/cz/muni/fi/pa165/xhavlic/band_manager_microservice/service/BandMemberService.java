package cz.muni.fi.pa165.xhavlic.band_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.BandMember;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.BandMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
public class BandMemberService {
    private final BandMemberRepository bandMemberRepository;

    private final BandService bandService;

    @Autowired
    public BandMemberService(BandMemberRepository bandMemberRepository, BandService bandService) {
        this.bandMemberRepository = bandMemberRepository;
        this.bandService = bandService;
    }


    public BandMember getBandMemberById(long id) {
        return bandMemberRepository.findBandMemberById(id)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, "band member with: " + id + " not found")
                );
    }

    @Transactional
    public BandMember updateBandMember(long id, BandMember bandMemberDetails) {
        int updatedCount = bandMemberRepository.updateBandMember(id, bandMemberDetails.getFirstName(), bandMemberDetails.getLastName());
        if (updatedCount == 0) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Band member with ID " + id + " not found");
        }

        return bandMemberRepository.findBandMemberById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Band member with ID " + id + " not found after update"));
    }

    @Transactional
    public BandMember createNewBandMember(BandMember bandMember) {
        return bandMemberRepository.save(bandMember);
    }

    @Transactional
    public void deleteBandMember(long id) {
        int affectedRows = bandMemberRepository.deleteBandMemberById(id);
        if (affectedRows < 1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "band member with: " + id + " not found");
        }
    }

    @Transactional
    @Modifying
    public BandMember addBandMemberToBand(long bandId, BandMember bandMember) {
        var createdBandMember = bandMemberRepository.save(bandMember);
        Band band = bandService.getBandById(bandId);
        band.getBandMembers().add(bandMember);
        return createdBandMember;
    }

}
