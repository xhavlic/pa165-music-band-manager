package cz.muni.fi.pa165.xhavlic.band_manager_microservice.api;

import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Jakub Uhlarik
 */

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@JsonRootName("Album")
public class AlbumDto {

    private Long id;

    @Schema(example = "Sicko mode",description = "name of album")
    private String name;

    @ArraySchema(schema = @Schema(implementation = SongDto.class, requiredMode = Schema.RequiredMode.NOT_REQUIRED))
    private List<SongDto> songs;
}
