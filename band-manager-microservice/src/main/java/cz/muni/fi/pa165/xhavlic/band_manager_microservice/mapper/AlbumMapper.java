package cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.AlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputAlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Jakub Uhlarik
 */
@Mapper(componentModel = "spring")
public interface AlbumMapper {

    AlbumDto mapToDto(Album album);

    Album mapFromDto(InputAlbumDto albumDto);

    List<AlbumDto> mapToList(List<Album> albums);
}
