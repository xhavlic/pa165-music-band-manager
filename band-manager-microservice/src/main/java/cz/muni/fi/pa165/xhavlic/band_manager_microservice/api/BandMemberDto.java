package cz.muni.fi.pa165.xhavlic.band_manager_microservice.api;

import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("BandMember")
public class BandMemberDto {

    private long id;

    @Schema(example = "John Lennon", requiredMode = Schema.RequiredMode.REQUIRED)
    private String firstName;

    @Schema(example = "Lennon", requiredMode = Schema.RequiredMode.REQUIRED)
    private String lastName;

}
