package cz.muni.fi.pa165.xhavlic.band_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Jakub Uhlarik
 */
@Service
public class AlbumService {

    private final BandService bandService;
    private final AlbumRepository albumRepository;

    @Autowired
    public AlbumService(BandService bandRepository, AlbumRepository albumRepository) {
        this.bandService = bandRepository;
        this.albumRepository = albumRepository;
    }

    public Album getAlbumById(long id) {
        return albumRepository.findAlbumById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "album with id" + id + "not found")
        );
    }

    public List<Album> getAllAlbumsForBandId(Long bandId) {
        return bandService.getBandById(bandId).getAlbums();
    }

    @Transactional
    public Album addAlbumToBand(Long bandId, Album album) {
        if (album.getName() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Album name cannot be null");
        }
        if (albumRepository.findAlbumByName(album.getName()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Album already exists: " + album.getName());
        }

        var createdAlbum = albumRepository.save(album);
        var band = bandService.getBandById(bandId);
        band.getAlbums().add(createdAlbum);
        return createdAlbum;
    }

    @Transactional
    public Album updateAlbum(long id, Album album) {
        var oldAlbum = albumRepository.findAlbumById(id);
        oldAlbum.orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "album with: " + id + " not found")
        );

        var oldAlbumValue = oldAlbum.get();

        if (!album.getSongs().isEmpty()){
            oldAlbumValue.setSongs(album.getSongs());
        }
        if (!album.getName().isEmpty()){
            oldAlbumValue.setName(album.getName());
        }
        albumRepository.save(oldAlbumValue);
        return oldAlbumValue;
    }

    @Transactional
    public List<Album> searchAlbums(String genre, String bandName, String albumName) {
        Set<Album> albums = new HashSet<>();

        if (bandName != null) {
            Band band = bandService.getBandByName(bandName);
            if (band != null) {
                albums.addAll(band.getAlbums());
            }
        }

        if (albumName != null) {
            List<Album> foundAlbums = albumRepository.findAlbumsByName(albumName);
            if (!foundAlbums.isEmpty()) {
                albums.addAll(foundAlbums);
            }
        }

        if (genre != null) {
            List<Band> bands = bandService.getBandsByGenre(genre);
            Set<Album> genreSpecificAlbums = bands.stream()
                    .flatMap(band -> band.getAlbums().stream())
                    .collect(Collectors.toSet());
            if (!genreSpecificAlbums.isEmpty()) {
                if (albums.isEmpty()) {
                    albums.addAll(genreSpecificAlbums);
                } else {
                    albums.retainAll(genreSpecificAlbums);
                }
            }
        }

        if (albums.isEmpty() && genre == null && bandName == null && albumName == null) {
            throw new IllegalArgumentException("At least one search parameter must be provided.");
        }

        return new ArrayList<>(albums);
    }
}
