package cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author Jakub Uhlarik
 */
@Repository
public interface AlbumRepository extends JpaRepository<Album,Long> {
    Optional<Album> findAlbumById(long id);
    Album save(Album album);
    @Transactional
    Optional<Album> findAlbumByName(String name);
    @Transactional
    List<Album> findAlbumsByName(String name);

}
