package cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputBandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.BandMember;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BandMemberMapper {

    BandMember mapFromDto(InputBandMemberDto bandMember);

    BandMemberDto mapToDto(BandMember bandMember);
}
