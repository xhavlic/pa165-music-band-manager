package cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputSongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.SongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Song;
import org.mapstruct.Mapper;


/**
 * @author Jakub Uhlarik
 */
@Mapper(componentModel = "spring")
public interface SongMapper {
    SongDto mapToDto(Song song);

    Song mapFromDto(InputSongDto songDto);

}
