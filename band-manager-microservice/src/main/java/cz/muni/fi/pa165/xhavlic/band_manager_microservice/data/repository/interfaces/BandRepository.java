package cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces;



import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author Jakub Uhlarik
 */
@Repository
public interface BandRepository extends JpaRepository<Band,Long> {
    Optional<Band> findBandById(long id);
    Band save(Band band);
    List<Band> findAll();
    int deleteBandById(long id);
    @Transactional
    Optional<Band> findBandByName(String name);
    @Transactional
    List<Band> getBandsByGenre(String genre);
}
