package cz.muni.fi.pa165.xhavlic.band_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputSongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.SongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade.SongFacade;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.oauth.BandServiceAuth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jakub Uhlarik
 */
@RestController
@RequestMapping("/api")
public class SongRestController {

    private final SongFacade songFacade;

    @Autowired
    public SongRestController(SongFacade songFacade) {
        this.songFacade = songFacade;
    }

    @Operation(summary = "Add song to band", description = "Add song from body to album determined by id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Song added to Album",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SongDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or song supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Album not found", content = @Content)})

    @PostMapping("/albums/{albumId}/songs")
    public ResponseEntity<SongDto> addSongToAlbum(@PathVariable long albumId, @RequestBody InputSongDto songDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(songFacade.addSongToAlbum(albumId, songDto));
    }



    @Operation(summary = "Get a song by id", description = "Returns a song with the id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the song",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SongDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Song not found", content = @Content)})
    @GetMapping("songs/{id}")
    public ResponseEntity<SongDto> getSongById(@PathVariable long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(songFacade.getSongById(id));
    }



    @Operation(summary = "Update a song", description = "Update song and returns id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update the song",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SongDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or song supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Song not found", content = @Content)})
    @PutMapping("songs/{id}")
    public ResponseEntity<SongDto> updateSong(@PathVariable long id, @RequestBody InputSongDto songDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(songFacade.updateSong(id, songDto));
    }
}
