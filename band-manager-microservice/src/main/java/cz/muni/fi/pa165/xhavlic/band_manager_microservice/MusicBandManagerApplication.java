package cz.muni.fi.pa165.xhavlic.band_manager_microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicBandManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MusicBandManagerApplication.class, args);

    }

}
