package cz.muni.fi.pa165.xhavlic.band_manager_microservice.config;

import com.github.javafaker.Faker;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.BandMember;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Song;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.AlbumRepository;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.BandMemberRepository;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.BandRepository;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.SongRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class DatabaseSeederConfig {

    private static final Faker faker = new Faker();
    private static final int numberOfBands = faker.number().numberBetween(5, 15);

    @Bean
    @Transactional
    public CommandLineRunner databaseSeeder(AlbumRepository albumRepository, BandRepository bandRepository,
                                            BandMemberRepository bandMemberRepository, SongRepository songRepository) {
        return args -> {

            String seedData = System.getProperty("seed.data", "false");
            String clearDb = System.getProperty("clear.db", "false");
            System.out.println("Seed data property: " + seedData);
            System.out.println("Clear DB property: " + clearDb);

            // Optionally clear the database
            if (Boolean.parseBoolean(clearDb)) {
                albumRepository.deleteAll();
                bandRepository.deleteAll();
                bandMemberRepository.deleteAll();
                songRepository.deleteAll();
            }
            if (Boolean.parseBoolean(seedData)) {
                // Generate bands
                List<Band> bands = generateBands();
                bandRepository.saveAll(bands);

                System.out.println("Database has been seeded");
            }
        };
    }

    private List<Band> generateBands() {
        List<Band> bands = new ArrayList<>();
        for (int i = 0; i < numberOfBands; i++) {
            Band band = new Band();
            band.setName(faker.rockBand().name());
            band.setGenre(faker.music().genre());
            band.setLogoUrl(faker.internet().avatar());
            band.setAlbums(generateAlbums());
            band.setBandMembers(generateBandMembers());
            bands.add(band);
        }
        return bands;
    }

    private List<Album> generateAlbums() {
        List<Album> albums = new ArrayList<>();
        for (int i = 0; i < faker.number().numberBetween(1, 5); i++) {
            Album album = new Album();
            album.setName(faker.lorem().sentence(faker.number().numberBetween(1, 4)));
            album.setSongs(generateSongs());
            albums.add(album);
        }
        return albums;
    }

    private List<Song> generateSongs() {
        List<Song> songs = new ArrayList<>();
        for (int i = 0; i < faker.number().numberBetween(1, 11); i++) {
            Song song = new Song();
            song.setName(faker.lorem().sentence(faker.number().numberBetween(1, 3)));
            song.setDuration(faker.number().numberBetween(180, 360));
            songs.add(song);
        }
        return songs;
    }

    private List<BandMember> generateBandMembers() {
        List<BandMember> members = new ArrayList<>();
        for (int i = 0; i < faker.number().numberBetween(1, 6); i++) {
            BandMember member = new BandMember();
            member.setFirstName(faker.name().firstName());
            member.setLastName(faker.name().lastName());
            members.add(member);
        }
        return members;
    }
}