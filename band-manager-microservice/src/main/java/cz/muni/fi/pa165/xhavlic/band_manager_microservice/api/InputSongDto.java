package cz.muni.fi.pa165.xhavlic.band_manager_microservice.api;

import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jakub Uhlarik
 */

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@JsonRootName("InputSong")
public class InputSongDto {

    @Schema(example = "100",description = "song durations in seconds")
    private int duration;

    @Schema(example = "Yosemite", description = "song name")
    private String name;
}
