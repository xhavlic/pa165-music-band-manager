package cz.muni.fi.pa165.xhavlic.band_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Song;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;


/**
 * @author Jakub Uhlarik
 */

@Service
public class SongService {

    private final SongRepository songRepository;
    private final AlbumService albumService;

    @Autowired
    public SongService(SongRepository songRepository, AlbumService albumService) {
        this.songRepository = songRepository;
        this.albumService = albumService;
    }

    @Transactional
    public Song addSongToAlbum(long albumId, Song song) {
        if (song.getName() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Song name cannot be null");
        }
        if (songRepository.findSongByName(song.getName()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Song already exists: " + song.getName());
        }
        var createdSong = songRepository.save(song);
        albumService.getAlbumById(albumId).getSongs().add(song);
        return createdSong;
    }
    @Transactional
    public Song getSongById(long id) {
        return songRepository.findSongById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "song with id: " + id + "not found")
        );
    }

    @Transactional
    public Song updateSong(long id, Song song) {
        int affectedRows = songRepository.updateSong(id, song.getDuration(),song.getName());
        if (affectedRows < 1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "song with id: " + id + "not found");
        }
        return songRepository.findSongById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Failed to retrieve updated song with ID " + id));
    }
}
