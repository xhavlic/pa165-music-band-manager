package cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.BandMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BandMemberRepository extends JpaRepository<BandMember, Long> {
    Optional<BandMember> findBandMemberById(long id);
    BandMember save(BandMember bandMember);
    List<BandMember> findAll();
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update BandMember b set b.firstName = :firstName, b.lastName = :lastName where b.id = :id")
    int updateBandMember(long id, String firstName, String lastName);
    int deleteBandMemberById(long id);
}
