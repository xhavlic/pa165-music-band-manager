package cz.muni.fi.pa165.xhavlic.band_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.BandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author Jakub Uhlarik
 */

@Service
public class BandService {

    private final BandRepository bandRepository;

    @Autowired
    public BandService(BandRepository bandRepository) {
        this.bandRepository = bandRepository;
    }

    public Band getBandById(long id) {
        return bandRepository.findBandById(id)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, "band with: " + id + " not found")
                );
    }

    @Transactional
    public Band updateBand(long id, Band band) {
        var oldBand = bandRepository.findBandById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "band with: " + id + " not found")
        );
        if (!band.getAlbums().isEmpty()) {
            oldBand.setAlbums(band.getAlbums());
        }
        if (!band.getBandMembers().isEmpty()) {
            oldBand.setBandMembers(band.getBandMembers());
        }
        if (!band.getName().isEmpty()) {
            oldBand.setName(band.getName());
        }
        if (!band.getGenre().isEmpty()) {
            oldBand.setGenre(band.getGenre());
        }
        if (!band.getLogoUrl().isEmpty()) {
            oldBand.setLogoUrl(band.getLogoUrl());
        }
        bandRepository.save(oldBand);
        return oldBand;
    }
    @Transactional
    public Band addNewBand(Band band) {
        if (band.getName() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Band name cannot be null");
        }
        if (bandRepository.findBandByName(band.getName()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Band already exists: " + band.getName());
        }
        return bandRepository.save(band);
    }

    @Transactional
    public List<Band> findAllBands() {
        return bandRepository.findAll();
    }

    @Transactional
    public void deleteBand(long id) {
        int deleteRows = bandRepository.deleteBandById(id);
        if (deleteRows < 1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "band with: " + id + " not found");
        }
    }

    @Transactional
    public List<Band> getBandsByGenre(String genre) {
        return bandRepository.getBandsByGenre(genre);
    }

    @Transactional
    public Band getBandByName(String name) {
        return bandRepository.findBandByName(name).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "band with: " + name + " not found")
        );
    }
}
