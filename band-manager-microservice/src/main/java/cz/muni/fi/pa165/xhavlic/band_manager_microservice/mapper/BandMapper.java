package cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.DetailedBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Jakub Uhlarik
 */

@Mapper(componentModel = "spring")
public interface BandMapper {

    BandDto mapToDto(Band band);

    Band mapFromDto(InputBandDto bandDto);

    DetailedBandDto mapToDetailedDto(Band band);

    List<BandDto> mapToList(List<Band> bands);
}
