package cz.muni.fi.pa165.xhavlic.band_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.AlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputAlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade.AlbumFacade;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.oauth.BandServiceAuth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Jakub Uhlarik
 */
@RestController
@RequestMapping("/api")
public class AlbumRestController {

    private final AlbumFacade albumFacade;

    public AlbumRestController(AlbumFacade albumFacade) {
        this.albumFacade = albumFacade;
    }

    @Operation(summary = "Get a album by id", description = "Returns a album with the id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2,scopes = {"test_read"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the album",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlbumDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Album not found", content = @Content)})
    @GetMapping("/albums/{id}")
    public ResponseEntity<AlbumDto> getAlbumById(@PathVariable long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(albumFacade.getAlbumById(id));
    }


    @Operation(summary = "Update a album", description = "Update album and returns id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2,scopes = {"test_write"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update the album",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlbumDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or album supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Album not found", content = @Content)})
    @PutMapping("/albums/{id}")
    public ResponseEntity<AlbumDto> updateAlbum(@PathVariable long id, @RequestBody InputAlbumDto albumDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(albumFacade.updateAlbum(id, albumDto));
    }

    @Operation(summary = "Get a albums from a band", description = "Returns a album per the id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2,scopes = {"test_read"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the album",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = AlbumDto.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Band not found", content = @Content)})
    @GetMapping("/bands/{bandId}/albums")
    public ResponseEntity<List<AlbumDto>> getAlbumsForBandId(@PathVariable long bandId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(albumFacade.getAlbumsForBandId(bandId));
    }

    @Operation(summary = "Add album to band", description = "Add album from body to band determined by id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2,scopes = {"test_write"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Album added to Band",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlbumDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or album supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Band not found", content = @Content)})
    @PostMapping("/bands/{bandId}/albums")
    public ResponseEntity<AlbumDto> addAlbumToBand(@PathVariable long bandId, @RequestBody InputAlbumDto albumDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(albumFacade.addAlbumToBand(bandId, albumDto));
    }

    @Operation(summary = "Search for albums", description = "Search albums by genre, band name, and album name.")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Search successful",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = AlbumDto.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid search parameters", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have the required scope test_read", content = @Content())
    })
    @GetMapping("/search/albums")
    public ResponseEntity<List<AlbumDto>> searchAlbums(
            @RequestParam(required = false) String genre,
            @RequestParam(required = false) String bandName,
            @RequestParam(required = false) String albumName) {

        List<AlbumDto> albums = albumFacade.searchAlbums(genre, bandName, albumName);
        return ResponseEntity.ok(albums);
    }
}
