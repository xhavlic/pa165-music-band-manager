package cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Song;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author Jakub Uhlarik
 */
@Repository
public interface SongRepository extends JpaRepository<Song,Long> {
    Optional<Song> findSongById(long id);
    Song save(Song song);
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update Song s set s.name = :name, s.duration = :duration where s.id = :id")
    int updateSong(long id, int duration, String name);
    @Transactional
    Optional<Song> findSongByName(String name);
}
