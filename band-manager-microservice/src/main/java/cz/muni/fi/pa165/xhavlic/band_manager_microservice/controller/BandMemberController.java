package cz.muni.fi.pa165.xhavlic.band_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputBandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade.BandMemberFacade;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.oauth.BandServiceAuth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class BandMemberController {

    private final BandMemberFacade bandMemberFacade;

    @Autowired
    public BandMemberController(BandMemberFacade bandMemberFacade) {
        this.bandMemberFacade = bandMemberFacade;
    }

    @Operation(summary = "Add member to band", description = "Add member from body to band determined by id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Added the band member",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BandMemberDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid body supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content())})
    @PostMapping("/bands/{id}/bandMember/")
    public ResponseEntity<BandMemberDto> addMemberToBand(@PathVariable long id, @RequestBody InputBandMemberDto bandMemberDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(bandMemberFacade.addBandMemberToBand(id, bandMemberDto));
    }

    @Operation(summary = "Get a band member by id", description = "Returns a band member with the id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_read"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the band member",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BandMemberDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Band member not found", content = @Content)})
    @GetMapping("/bandMembers/{id}")
    public ResponseEntity<BandMemberDto> getBandMemberById(@PathVariable long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(bandMemberFacade.getBandMemberById(id));
    }

    @Operation(summary = "Update a band member", description = "Update band member and returns id")
    @SecurityRequirement(name = BandServiceAuth.SECURITY_SCHEME_OAUTH2, scopes = {"test_write"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update the band member",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BandMemberDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or band member supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Band member not found", content = @Content)})
    @PutMapping("/bandMembers/{id}")
    public ResponseEntity<BandMemberDto> updateBandMember(@PathVariable long id, @RequestBody InputBandMemberDto bandMemberDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(bandMemberFacade.updateBandMember(id, bandMemberDto));
    }
}
