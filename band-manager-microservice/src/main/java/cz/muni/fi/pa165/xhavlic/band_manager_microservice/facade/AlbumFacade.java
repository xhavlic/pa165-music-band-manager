package cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.AlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputAlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper.AlbumMapper;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jakub Uhlarik
 */
@Service
public class AlbumFacade {

    private final AlbumService albumService;

    private final AlbumMapper albumMapper;

    @Autowired
    public AlbumFacade(AlbumService albumService, AlbumMapper albumMapper) {
        this.albumService = albumService;
        this.albumMapper = albumMapper;
    }

    public AlbumDto getAlbumById(long id) {
        return albumMapper.mapToDto(albumService.getAlbumById(id));
    }

    public List<AlbumDto> getAlbumsForBandId(long bandId) {
        return albumMapper.mapToList(albumService.getAllAlbumsForBandId(bandId));
    }

    public AlbumDto addAlbumToBand(Long bandId, InputAlbumDto album) {
        return albumMapper.mapToDto(albumService.addAlbumToBand(bandId, albumMapper.mapFromDto(album)));
    }


    public AlbumDto updateAlbum(Long id, InputAlbumDto album) {
        return albumMapper.mapToDto(albumService.updateAlbum(id, albumMapper.mapFromDto(album)));
    }

    public List<AlbumDto> searchAlbums(String genre, String bandName, String albumName) {
        return albumMapper.mapToList(albumService.searchAlbums(genre, bandName, albumName));
    }
}
