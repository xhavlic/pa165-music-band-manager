package cz.muni.fi.pa165.xhavlic.band_manager_microservice.api;

import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jakub Uhlarik
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("Band")
public class BandDto {

    private long id;

    @Schema(example = "www.image.com", requiredMode = Schema.RequiredMode.REQUIRED)
    private String logoUrl;

    @Schema(example = "rock", requiredMode = Schema.RequiredMode.REQUIRED)
    private String genre;

    @Schema(example = "AC/DC", requiredMode = Schema.RequiredMode.REQUIRED)
    private String name;

}
