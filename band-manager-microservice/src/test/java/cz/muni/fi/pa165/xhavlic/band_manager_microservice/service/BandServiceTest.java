package cz.muni.fi.pa165.xhavlic.band_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.BandMember;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.BandRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BandServiceTest {

    @Mock
    private BandRepository bandRepository;

    @InjectMocks
    private BandService bandService;

    private Band band;

    @BeforeEach
    void setUp() {

        List<Album> albums = new ArrayList<>();
        List<BandMember> members = new ArrayList<>();
        band = new Band(1L, "http://example.com/logo.png", "Rock", "The Test Band", albums, members);
    }

    @Test
    void getBandByIdShouldReturnBand() {
        when(bandRepository.findBandById(band.getId())).thenReturn(Optional.of(band));

        Band foundBand = bandService.getBandById(band.getId());

        assertThat(foundBand).isEqualTo(band);
        verify(bandRepository).findBandById(band.getId());
    }

    @Test
    void getBandByIdShouldThrowNotFound() {
        long nonExistentId = 999L;
        when(bandRepository.findBandById(nonExistentId)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> bandService.getBandById(nonExistentId))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessageContaining("not found")
                .hasFieldOrPropertyWithValue("status", HttpStatus.NOT_FOUND);
    }

    @Test
    void updateBandShouldReturnUpdatedBand() {
        when(bandRepository.findBandById(eq(band.getId()))).thenReturn(Optional.of(band));

        Band updatedBand = bandService.updateBand(band.getId(), band);

        assertThat(updatedBand).isEqualTo(band);
        verify(bandRepository).findBandById(eq(band.getId()));
    }

    @Test
    void addNewBandShouldReturnNewBandId() {
        Band newBand = new Band();
        newBand.setName("AAAA");

        Band savedBand = new Band();
        savedBand.setId(1L);

        when(bandRepository.save(any(Band.class))).thenReturn(savedBand);

        var createdBand = bandService.addNewBand(newBand);

        assertThat(createdBand).isEqualTo(savedBand);
        verify(bandRepository).save(any(Band.class));
    }

    @Test
    void findAllBandsShouldReturnListOfBands() {
        Band anotherBand = new Band();
        // Setup anotherBand as needed
        List<Band> expectedBands = Arrays.asList(band, anotherBand);

        when(bandRepository.findAll()).thenReturn(expectedBands);

        List<Band> bands = bandService.findAllBands();

        assertThat(bands).hasSize(2).containsExactlyInAnyOrderElementsOf(expectedBands);
        verify(bandRepository).findAll();
    }

    @Test
    void deleteBandShouldNotThrowException() {
        when(bandRepository.deleteBandById(band.getId())).thenReturn(1);

        bandService.deleteBand(band.getId());

        verify(bandRepository).deleteBandById(band.getId());
    }

    @Test
    void deleteBandShouldThrowNotFound() {
        long nonExistentId = 999L;
        when(bandRepository.deleteBandById(nonExistentId)).thenReturn(0);

        assertThatThrownBy(() -> bandService.deleteBand(nonExistentId))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessageContaining("not found")
                .hasFieldOrPropertyWithValue("status", HttpStatus.NOT_FOUND);
    }
}