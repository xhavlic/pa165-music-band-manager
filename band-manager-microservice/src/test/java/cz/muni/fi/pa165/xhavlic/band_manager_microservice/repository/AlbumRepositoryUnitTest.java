package cz.muni.fi.pa165.xhavlic.band_manager_microservice.repository;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.AlbumRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
public class AlbumRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AlbumRepository albumRepository;

    @Test
    public void testFindById() {
        Album album = new Album();
        album.setName("Test Album");
        album = entityManager.persistAndFlush(album);

        Optional<Album> found = albumRepository.findAlbumById(album.getId());

        assertThat(found.isPresent()).isTrue();
        assertThat(found.get().getName()).isEqualTo("Test Album");
    }
    @Test
    public void testSave() {
        Album album = new Album();
        album.setName("New Album");

        Album savedAlbum = albumRepository.save(album);

        assertThat(savedAlbum).isNotNull();
        assertThat(savedAlbum.getId()).isNotNull();
        assertThat(savedAlbum.getName()).isEqualTo("New Album");
    }
    @Test
    public void testFindByIdNotFound() {
        Optional<Album> result = albumRepository.findAlbumById(999L);

        assertThat(result).isNotPresent();
    }
    @Test
    public void testDelete() {
        Album album = new Album();
        album.setName("Album to Delete");
        album = entityManager.persistAndFlush(album);

        albumRepository.delete(album);
        entityManager.flush();

        Optional<Album> result = albumRepository.findById(album.getId());
        assertThat(result).isNotPresent();
    }
    @Test
    public void testUpdate() {
        Album album = new Album();
        album.setName("Original Name");
        album = entityManager.persistAndFlush(album);

        album.setName("Updated Name");
        albumRepository.save(album);
        entityManager.flush();

        Optional<Album> updatedAlbum = albumRepository.findById(album.getId());
        assertThat(updatedAlbum).isPresent();
        assertThat(updatedAlbum.get().getName()).isEqualTo("Updated Name");
    }
    @Test
    public void testFindAlbumsByName() {
        Album album = new Album();
        album.setName("The Dark Side of the Moon");
        entityManager.persist(album);
        entityManager.flush();

        Optional<Album> foundAlbum = albumRepository.findAlbumByName("The Dark Side of the Moon");

        assertThat(foundAlbum.isPresent()).isTrue();
        assertThat(foundAlbum.get().getName()).isEqualTo("The Dark Side of the Moon");
    }
}