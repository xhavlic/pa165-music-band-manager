package cz.muni.fi.pa165.xhavlic.band_manager_microservice.repository;


import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.BandRepository;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;


@DataJpaTest
@ActiveProfiles("test")
public class BandRepositoryUnitTest {
    @Autowired
    private BandRepository bandRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    void findBandByIdShouldReturnBand() {
        Band band = new Band();
        band.setName("The Beatles");
        entityManager.persistAndFlush(band);

        Optional<Band> found = bandRepository.findBandById(band.getId());
        assertThat(found).isPresent();
        assertThat(found.get()).isEqualTo(band);
    }
    @Test
    void saveShouldPersistBand() {
        Band band = new Band();
        band.setName("Nirvana");
        Band savedBand = bandRepository.save(band);
        assertThat(savedBand).isNotNull();
        assertThat(savedBand.getName()).isEqualTo("Nirvana");
    }
    @Test
    void findAllShouldReturnAllBands() {
        Band band1 = new Band();
        band1.setName("The Rolling Stones");
        Band band2 = new Band();
        band2.setName("AC/DC");
        entityManager.persist(band1);
        entityManager.persist(band2);
        entityManager.flush();

        List<Band> bands = bandRepository.findAll();
        assertThat(bands).hasSize(2);
        assertThat(bands).containsExactlyInAnyOrder(band1, band2);
    }
    @Test
    void deleteBandByIdShouldChangeDatabaseState() {
        Band band = new Band();
        band.setName("Metallica");
        entityManager.persistAndFlush(band);

        bandRepository.deleteById(band.getId());
        Optional<Band> found = bandRepository.findBandById(band.getId());
        assertThat(found).isEmpty();
    }
    @Test
    void findBandByIdShouldReturnEmptyWhenNotFound() {
        Optional<Band> result = bandRepository.findBandById(99L);
        assertThat(result).isEmpty();
    }
    @Test
    void deleteBandByIdShouldReturnZeroWhenIdNotFound() {
        int deletedCount = bandRepository.deleteBandById(100L);
        assertThat(deletedCount).isEqualTo(0);
    }

    @Test
    public void testGetBandsByGenre() {
        Band band = new Band();
        band.setName("Pink Floyd");
        band.setGenre("Rock");
        entityManager.persist(band);
        entityManager.flush();

        List<Band> bands = bandRepository.getBandsByGenre("Rock");

        assertThat(bands).isNotEmpty();
        assertThat(bands.getFirst().getGenre()).isEqualTo("Rock");
    }
}
