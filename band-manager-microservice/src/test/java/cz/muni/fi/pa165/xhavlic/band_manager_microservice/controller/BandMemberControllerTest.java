package cz.muni.fi.pa165.xhavlic.band_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade.BandMemberFacade;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BandMemberControllerTest {

    @Mock
    private BandMemberFacade bandMemberFacade;

    @InjectMocks
    private BandMemberController bandMemberController;

    @Test
    void addMemberToBandShouldReturnMember() {
        BandMemberDto bandMemberDto = TestDataFactory.bandMemberDto;
        var inputBandMemberDto = TestDataFactory.inputBandMemberDto;

        when(bandMemberFacade.addBandMemberToBand(eq(bandMemberDto.getId()), eq(inputBandMemberDto)))
                .thenReturn(bandMemberDto);

        ResponseEntity<BandMemberDto> response = bandMemberController.addMemberToBand(bandMemberDto.getId(), inputBandMemberDto);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isEqualTo(bandMemberDto);
    }

    @Test
    void getBandMemberByIdShouldReturnBandMember() {
        BandMemberDto expectedBandMember = TestDataFactory.bandMemberDto;

        when(bandMemberFacade.getBandMemberById(anyLong())).thenReturn(expectedBandMember);

        ResponseEntity<BandMemberDto> response = bandMemberController.getBandMemberById(expectedBandMember.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(expectedBandMember);
    }

    @Test
    void updateBandMemberShouldReturnUpdatedBandMember() {
        BandMemberDto bandMemberDto = new BandMemberDto(1L, "Updated Name", "Updated Lastname");
        var inputBandMemberDto = TestDataFactory.inputBandMemberDto;

        when(bandMemberFacade.updateBandMember(eq(bandMemberDto.getId()), eq(inputBandMemberDto))).thenReturn(bandMemberDto);

        ResponseEntity<BandMemberDto> response = bandMemberController.updateBandMember(bandMemberDto.getId(), inputBandMemberDto);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(bandMemberDto);
    }
}