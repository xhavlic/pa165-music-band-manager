package cz.muni.fi.pa165.xhavlic.band_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.AlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputAlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputSongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.SongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade.AlbumFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AlbumRestControllerTest {

    @Mock
    private AlbumFacade albumFacade;

    @InjectMocks
    private AlbumRestController albumRestController;

    private AlbumDto createTestAlbumDto(Long id) {
        List<SongDto> songs = Arrays.asList(
                new SongDto(1L, 210, "Song 1"),
                new SongDto(2L, 180, "Song 2" )
        );
        return new AlbumDto(id, "Sicko Mode", songs);
    }
    private InputAlbumDto createTestInputAlbumDto() {
        List<InputSongDto> songs = Arrays.asList(
                new InputSongDto( 210, "Song 1"),
                new InputSongDto( 180, "Song 2" )
        );
        return new InputAlbumDto( "Sicko Mode", songs);
    }

    @Test
    void getAlbumByIdShouldReturnAlbum() {
        long id = 1L;
        AlbumDto expectedAlbum = createTestAlbumDto(id);

        when(albumFacade.getAlbumById(id)).thenReturn(expectedAlbum);

        var result = albumRestController.getAlbumById(id);

        assertThat(result.getBody()).isEqualTo(expectedAlbum);
        verify(albumFacade).getAlbumById(id);
    }

    @Test
    void updateAlbumShouldReturnUpdatedAlbum() {
        long id = 1L;
        AlbumDto albumDto = createTestAlbumDto(id);
        var  inputAlbumDto = createTestInputAlbumDto();

        when(albumFacade.updateAlbum(id, inputAlbumDto)).thenReturn(albumDto);

        var result = albumRestController.updateAlbum(id, inputAlbumDto);

        assertThat(result.getBody()).isEqualTo(albumDto);
        verify(albumFacade).updateAlbum(id, inputAlbumDto);
    }

    @Test
    void getAlbumsForBandIdShouldReturnAlbums() {
        long bandId = 1L;
        List<AlbumDto> albums = Arrays.asList(createTestAlbumDto(1L), createTestAlbumDto(2L));

        when(albumFacade.getAlbumsForBandId(bandId)).thenReturn(albums);

        var result = albumRestController.getAlbumsForBandId(bandId);

        assertThat(result.getBody()).isEqualTo(albums);
        verify(albumFacade).getAlbumsForBandId(bandId);
    }

    @Test
    void addAlbumToBandShouldReturnAlbum() {
        Long bandId = 1L;
        var  albumDto = createTestAlbumDto(null);
        var  inputAlbumDto = createTestInputAlbumDto();

        when(albumFacade.addAlbumToBand(bandId, inputAlbumDto)).thenReturn(albumDto);

        var result = albumRestController.addAlbumToBand(bandId, inputAlbumDto);

        assertThat(result.getBody()).isEqualTo(albumDto);
        verify(albumFacade).addAlbumToBand(bandId, inputAlbumDto);
    }
    @Test
    void searchAlbumsShouldReturnAlbums() {
        String genre = "Rock";
        String bandName = "Queen";
        String albumName = "Greatest Hits";
        List<AlbumDto> expectedAlbums = List.of(
                new AlbumDto(1L, "Greatest Hits", Collections.emptyList())
        );

        when(albumFacade.searchAlbums(genre, bandName, albumName)).thenReturn(expectedAlbums);

        ResponseEntity<List<AlbumDto>> response = albumRestController.searchAlbums(genre, bandName, albumName);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(expectedAlbums);
        verify(albumFacade).searchAlbums(genre, bandName, albumName);
    }

    @Test
    void searchAlbumsWhenNoResults() {
        String genre = "Rock";
        String bandName = "Unknown Band";
        String albumName = "Unknown Album";

        when(albumFacade.searchAlbums(genre, bandName, albumName)).thenReturn(Collections.emptyList());

        ResponseEntity<List<AlbumDto>> response = albumRestController.searchAlbums(genre, bandName, albumName);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEmpty();
        verify(albumFacade).searchAlbums(genre, bandName, albumName);
    }

    @Test
    void searchAlbumsWithNullParametersShouldHandleGracefully() {
        ResponseEntity<List<AlbumDto>> response = albumRestController.searchAlbums(null, null, null);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEmpty();
        verify(albumFacade).searchAlbums(null, null, null);
    }
}