
package cz.muni.fi.pa165.xhavlic.band_manager_microservice.mvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BandIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @AfterAll
    public void dropDb(){
        jdbcTemplate.execute("DROP ALL OBJECTS");
    }

    private BandDto createBand(String name) throws Exception {
        BandDto bandDto = new BandDto();
        bandDto.setName(name);

        String responseJson = mockMvc.perform(post("/api/bands")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(bandDto)))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        return objectMapper.readValue(responseJson, BandDto.class);

    }
    @Test
    public void createBandTest() throws Exception {
        BandDto bandDto = new BandDto();
        bandDto.setName("Nirvana");

        mockMvc.perform(post("/api/bands")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(bandDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Nirvana"))
                .andExpect(jsonPath("$.id").isNotEmpty());

    }
    @Test
    void getAllBands() throws Exception {
        createBand("The Beatles");
        createBand("Led Zeppelin");

        MvcResult result = mockMvc.perform(get("/api/bands"))
                .andExpect(status().isOk())
                .andReturn();

        List<Band> bands = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});
        assertTrue(bands.stream().anyMatch(band -> band.getName().equals("The Beatles")));
        assertTrue(bands.stream().anyMatch(band -> band.getName().equals("Led Zeppelin")));
    }
    @Test
    void deleteBand() throws Exception {
        BandDto band = createBand("Queen");

        mockMvc.perform(delete("/api/bands/" + band.getId()))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/bands/" + band.getId()))
                .andExpect(status().isNotFound());
    }
}