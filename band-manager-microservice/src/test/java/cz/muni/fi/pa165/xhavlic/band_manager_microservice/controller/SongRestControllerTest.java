package cz.muni.fi.pa165.xhavlic.band_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.SongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade.SongFacade;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class SongRestControllerTest {

    @Mock
    private SongFacade songFacade;

    @InjectMocks
    private SongRestController songRestController;

    private SongDto createTestSongDto(Long id) {
        return new SongDto(id, 180, "Yosemite");
    }

    @Test
    void addSongToAlbumShouldReturnSong() {
        long albumId = 1L;
        SongDto newSong = createTestSongDto(null);
        var inputSong = TestDataFactory.inputSongDto;

        when(songFacade.addSongToAlbum(albumId, inputSong)).thenReturn(newSong);

        var response = songRestController.addSongToAlbum(albumId, inputSong);

        assertThat(response.getBody()).isEqualTo(newSong);
        verify(songFacade).addSongToAlbum(albumId, inputSong);
    }

    @Test
    void getSongByIdShouldReturnSong() {
        long songId = 1L;
        SongDto expectedSong = createTestSongDto(songId);

        when(songFacade.getSongById(songId)).thenReturn(expectedSong);

        var response = songRestController.getSongById(songId);

        assertThat(response.getBody()).isEqualTo(expectedSong);
        verify(songFacade).getSongById(songId);
    }

    @Test
    void updateSongShouldReturnUpdatedSong() {
        long songId = 1L;
        SongDto updatedSong = createTestSongDto(songId);
        var inputSong = TestDataFactory.inputSongDto;

        when(songFacade.updateSong(songId, inputSong)).thenReturn(updatedSong);

        var response = songRestController.updateSong(songId, inputSong);

        assertThat(response.getBody()).isEqualTo(updatedSong);
        verify(songFacade).updateSong(songId, inputSong);
    }
}