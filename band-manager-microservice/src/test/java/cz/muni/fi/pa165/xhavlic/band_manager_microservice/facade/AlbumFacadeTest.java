package cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.AlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper.AlbumMapper;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.service.AlbumService;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.util.TestDataFactory;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
class AlbumFacadeTest {

    @InjectMocks
    private AlbumFacade albumFacade;

    @Mock
    private AlbumService albumService;

    @Mock
    private AlbumMapper albumMapper;


    @Nested
    class getAlbumById {
        @Test
        void shouldReturnAlbum() {
            when(albumService.getAlbumById(1L)).thenReturn(TestDataFactory.album);
            when(albumMapper.mapToDto(any())).thenReturn(TestDataFactory.albumDto);

            AlbumDto albumById = albumFacade.getAlbumById(1L);

            assertThat(albumById).isEqualTo(TestDataFactory.albumDto);
        }
    }

    @Nested
    class getAlbumsForBandId {
        @Test
        void shouldReturnAlbums() {
            when(albumService.getAllAlbumsForBandId(1L)).thenReturn(TestDataFactory.albums);
            when(albumMapper.mapToList(any())).thenReturn(TestDataFactory.albumDtos);

            assertThat(albumFacade.getAlbumsForBandId(1L)).isEqualTo(TestDataFactory.albumDtos);
        }
    }

    @Nested
    class addAlbumToBand {
        @Test
        void shouldReturnDtoOfAddedAlbum() {
            when(albumService.addAlbumToBand(1L, TestDataFactory.album)).thenReturn(TestDataFactory.album);
            when(albumMapper.mapFromDto(any())).thenReturn(TestDataFactory.album);
            when(albumMapper.mapToDto(any())).thenReturn(TestDataFactory.albumDto);

            AlbumDto albumDto = albumFacade.addAlbumToBand(1L, TestDataFactory.inputAlbumDto);

            assertEquals(albumDto, TestDataFactory.albumDto);
        }
    }

    @Nested
    class updateAlbum {
        @Test
        void shouldReturnUpdatedAlbum() {
            when(albumService.updateAlbum(1L, TestDataFactory.album)).thenReturn(TestDataFactory.album);
            when(albumMapper.mapToDto(any())).thenReturn(TestDataFactory.albumDto);
            when(albumMapper.mapFromDto(any())).thenReturn(TestDataFactory.album);

            AlbumDto updatedAlbum = albumFacade.updateAlbum(1L, TestDataFactory.inputAlbumDto);

            assertThat(updatedAlbum).isEqualTo(TestDataFactory.albumDto);
        }
    }
    @Nested
    class SearchAlbums {
        @Test
        void shouldReturnFilteredAlbums() {
            String genre = "Rock";
            String bandName = "The Rolling Stones";
            String albumName = "Sticky Fingers";
            List<Album> serviceAlbums = List.of(new Album(1L, albumName, new ArrayList<>()));
            List<AlbumDto> expectedDtos = List.of(new AlbumDto(1L, albumName, new ArrayList<>()));

            when(albumService.searchAlbums(genre, bandName, albumName)).thenReturn(serviceAlbums);

            when(albumMapper.mapToList(serviceAlbums)).thenReturn(expectedDtos);

            List<AlbumDto> resultDtos = albumFacade.searchAlbums(genre, bandName, albumName);

            assertThat(resultDtos).hasSize(1);
            assertThat(resultDtos).isEqualTo(expectedDtos);
        }

        @Test
        void shouldReturnEmptyListWhenNoAlbumsFound() {
            String genre = "Jazz";
            String bandName = "Unknown Band";
            String albumName = "Unknown Album";

            when(albumService.searchAlbums(genre, bandName, albumName)).thenReturn(List.of());

            when(albumMapper.mapToList(any())).thenReturn(List.of());

            List<AlbumDto> resultDtos = albumFacade.searchAlbums(genre, bandName, albumName);

            assertThat(resultDtos).isEmpty();
        }
    }


}