package cz.muni.fi.pa165.xhavlic.band_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.BandMember;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.BandMemberRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BandMemberServiceTest {

    @Mock
    private BandMemberRepository bandMemberRepository;

    @Mock
    private BandService bandService;

    @InjectMocks
    private BandMemberService bandMemberService;

    private BandMember bandMember;
    private Band band;

    @BeforeEach
    void setUp() {
        bandMember = new BandMember(1L, "John", "Doe");
        List<Album> albums = new ArrayList<>();
        List<BandMember> members = new ArrayList<>();
        band = new Band(1L, "http://example.com/logo.png", "Rock", "The Test Band", albums, members);
    }

    @Test
    void getBandMemberByIdShouldReturnBandMember() {
        when(bandMemberRepository.findBandMemberById(bandMember.getId())).thenReturn(Optional.of(bandMember));

        BandMember found = bandMemberService.getBandMemberById(bandMember.getId());

        assertThat(found).isEqualTo(bandMember);
        verify(bandMemberRepository).findBandMemberById(bandMember.getId());
    }

    @Test
    void getBandMemberByIdShouldThrowNotFound() {
        long id = 999L;
        when(bandMemberRepository.findBandMemberById(id)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> bandMemberService.getBandMemberById(id))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessageContaining("not found")
                .hasFieldOrPropertyWithValue("status", HttpStatus.NOT_FOUND);
    }

    @Test
    void updateBandMemberShouldReturnUpdatedBandMember() {
        BandMember bandMember = new BandMember();
        bandMember.setId(1L);
        bandMember.setFirstName("John");
        bandMember.setLastName("Doe");

        when(bandMemberRepository.updateBandMember(bandMember.getId(), bandMember.getFirstName(), bandMember.getLastName())).thenReturn(1);
        when(bandMemberRepository.findBandMemberById(bandMember.getId())).thenReturn(Optional.of(bandMember));

        BandMember updated = bandMemberService.updateBandMember(bandMember.getId(), bandMember);

        assertThat(updated).isEqualTo(bandMember);
        verify(bandMemberRepository).updateBandMember(bandMember.getId(), bandMember.getFirstName(), bandMember.getLastName());
        verify(bandMemberRepository).findBandMemberById(bandMember.getId());
    }

    @Test
    void createNewBandMemberShouldReturnId() {
        when(bandMemberRepository.save(bandMember)).thenReturn(bandMember);

        var createdId = bandMemberService.createNewBandMember(bandMember);

        assertThat(createdId).isEqualTo(bandMember);
        verify(bandMemberRepository).save(bandMember);
    }

    @Test
    void deleteBandMemberShouldNotThrowException() {
        when(bandMemberRepository.deleteBandMemberById(bandMember.getId())).thenReturn(1);

        bandMemberService.deleteBandMember(bandMember.getId());

        verify(bandMemberRepository).deleteBandMemberById(bandMember.getId());
    }

    @Test
    void addBandMemberToBandShouldReturnMember() {
        when(bandMemberRepository.save(bandMember)).thenReturn(bandMember);
        when(bandService.getBandById(anyLong())).thenReturn(band);

        var newMember = bandMemberService.addBandMemberToBand(band.getId(), bandMember);

        assertThat(newMember).isEqualTo(bandMember);
        assertThat(band.getBandMembers()).contains(bandMember);
        verify(bandMemberRepository).save(bandMember);
        verify(bandService).getBandById(band.getId());
    }
}