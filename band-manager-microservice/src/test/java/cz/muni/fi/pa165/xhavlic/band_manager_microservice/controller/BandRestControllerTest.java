package cz.muni.fi.pa165.xhavlic.band_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.AlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.DetailedBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade.BandFacade;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BandRestControllerTest {

    @Mock
    private BandFacade bandFacade;

    @InjectMocks
    private BandRestController bandRestController;

    private BandDto createTestBandDto() {
        return new BandDto(1L, "www.image.com", "rock", "AC/DC");
    }

    private DetailedBandDto createTestDetailedBandDto() {
        List<AlbumDto> albums = new ArrayList<>();
        List<BandMemberDto> members = new ArrayList<>();
        return new DetailedBandDto(1L, "www.image.com", "rock", "AC/DC", albums, members);
    }

    @Test
    void getBandByIdShouldReturnBand() {
        long id = 1L;
        DetailedBandDto expected = createTestDetailedBandDto();

        when(bandFacade.getBandById(id)).thenReturn(expected);

        var result = bandRestController.getBandById(id);

        assertThat(result.getBody()).isEqualTo(expected);
        verify(bandFacade).getBandById(id);
    }

    @Test
    void addNewBandShouldReturnBand() {
        BandDto newBand = createTestBandDto();
        var inputBand = TestDataFactory.inputBandDto;

        when(bandFacade.addNewBand(inputBand)).thenReturn(newBand);

        var result = bandRestController.addNewBand(inputBand);

        assertThat(result.getBody()).isEqualTo(newBand);
        verify(bandFacade).addNewBand(inputBand);
    }

    @Test
    void listAllBandsShouldReturnListOfBands() {
        List<BandDto> expectedBands = List.of(createTestBandDto());

        when(bandFacade.findAllBands()).thenReturn(expectedBands);

        var result = bandRestController.listAllBands();

        assertThat(result.getBody()).isEqualTo(expectedBands);
        verify(bandFacade).findAllBands();
    }

    @Test
    void updateBandShouldUpdateAndReturnBand() {
        long id = 1L;
        BandDto updatedBand = createTestBandDto();
        var inputBand = TestDataFactory.inputBandDto;

        when(bandFacade.updateBand(id, inputBand)).thenReturn(updatedBand);

        var result = bandRestController.updateBand(id, inputBand);

        assertThat(result.getBody()).isEqualTo(updatedBand);
        verify(bandFacade).updateBand(id, inputBand);
    }

    @Test
    void deleteBandShouldReturnNoContent() {
        long id = 1L;

        bandRestController.deleteBand(id);

        verify(bandFacade).deleteBand(id);
    }
}
