package cz.muni.fi.pa165.xhavlic.band_manager_microservice.repository;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.BandMember;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.BandMemberRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
public class BandMemberRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BandMemberRepository bandMemberRepository;

    @Test
    public void testFindById() {
        BandMember bandMember = new BandMember();
        bandMember.setFirstName("John");
        bandMember.setLastName("Doe");
        bandMember = entityManager.persistFlushFind(bandMember);

        Optional<BandMember> foundMember = bandMemberRepository.findBandMemberById(bandMember.getId());

        assertThat(foundMember).isPresent();
        assertThat(foundMember.get().getFirstName()).isEqualTo("John");
        assertThat(foundMember.get().getLastName()).isEqualTo("Doe");
    }
    @Test
    public void testSave() {
        BandMember bandMember = new BandMember();
        bandMember.setFirstName("Alice");
        bandMember.setLastName("Cooper");

        BandMember savedMember = bandMemberRepository.save(bandMember);

        assertThat(savedMember).isNotNull();
        assertThat(savedMember.getId()).isNotNull();
    }
    @Test
    public void testFindAll() {
        BandMember bandMember1 = new BandMember();
        bandMember1.setFirstName("Freddie");
        bandMember1.setLastName("Mercury");
        entityManager.persist(bandMember1);

        BandMember bandMember2 = new BandMember();
        bandMember2.setFirstName("Brian");
        bandMember2.setLastName("May");
        entityManager.persist(bandMember2);

        entityManager.flush();

        List<BandMember> members = bandMemberRepository.findAll();

        assertThat(members).hasSize(2);
    }
    @Test
    public void testUpdateBandMember() {
        BandMember bandMember = new BandMember();
        bandMember.setFirstName("David");
        bandMember.setLastName("Bowie");
        entityManager.persistAndFlush(bandMember);

        int updatedCount = bandMemberRepository.updateBandMember(bandMember.getId(), "David", "Jones");
        assertThat(updatedCount).isEqualTo(1);

        BandMember updatedBandMember = entityManager.find(BandMember.class, bandMember.getId());
        assertThat(updatedBandMember.getLastName()).isEqualTo("Jones");
    }
    @Test
    public void testDeleteBandMemberById() {
        BandMember bandMember = new BandMember();
        bandMember.setFirstName("Mick");
        bandMember.setLastName("Jagger");
        bandMember = entityManager.persistFlushFind(bandMember);

        bandMemberRepository.deleteBandMemberById(bandMember.getId());
        entityManager.flush();

        Optional<BandMember> result = bandMemberRepository.findById(bandMember.getId());
        assertThat(result).isNotPresent();
    }
}