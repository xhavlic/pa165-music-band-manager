package cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.DetailedBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper.BandMapper;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.service.BandService;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.util.TestDataFactory;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
class BandFacadeTest {
    @InjectMocks
    private BandFacade bandFacade;
    @Mock
    private BandService bandService;
    @Mock
    private BandMapper bandMapper;

    @Nested
    class getBandById {
        @Test
        void shouldReturnBand() {
            Mockito.when(bandMapper.mapToDetailedDto(Mockito.any())).thenReturn(TestDataFactory.detailedBandDto);

            Mockito.when(bandService.getBandById(4L)).thenReturn(TestDataFactory.band);

            DetailedBandDto bandById = bandFacade.getBandById(4L);

            assertThat(bandById).isEqualTo(TestDataFactory.detailedBandDto);
        }
    }

    @Nested
    class addNewBand {
        @Test
        void shouldReturnAddedBand() {
            Mockito.when(bandMapper.mapFromDto(Mockito.any())).thenReturn(TestDataFactory.band);
            Mockito.when(bandService.addNewBand(TestDataFactory.band)).thenReturn(TestDataFactory.band);
            Mockito.when(bandMapper.mapToDto(TestDataFactory.band)).thenReturn(TestDataFactory.bandDto);

            assertThat(bandFacade.addNewBand(TestDataFactory.inputBandDto)).isEqualTo(TestDataFactory.bandDto);

        }
    }

    @Nested
    class findAllBands {
        @Test
        void shouldReturnAllBands() {
            Mockito.when(bandService.findAllBands()).thenReturn(TestDataFactory.bands);
            Mockito.when(bandMapper.mapToList(TestDataFactory.bands)).thenReturn(TestDataFactory.bandsDtos);

            assertThat(bandFacade.findAllBands()).isEqualTo(TestDataFactory.bandsDtos);
        }
    }

    @Nested
    class updateBand {
        @Test
        void shouldReturnUpdatedBand() {
            Mockito.when(bandMapper.mapFromDto(Mockito.any())).thenReturn(TestDataFactory.band);
            Mockito.when(bandService.updateBand(1L, TestDataFactory.band)).thenReturn(TestDataFactory.band);
            Mockito.when(bandMapper.mapToDto(TestDataFactory.band)).thenReturn(TestDataFactory.bandDto);

            assertThat(bandFacade.updateBand(1L, TestDataFactory.inputBandDto)).isEqualTo(TestDataFactory.bandDto);
        }
    }

    @Nested
    class deleteBand {
        @Test
        void shouldDeleteBand() {
            Mockito.doNothing().when(bandService).deleteBand(4L);

            bandFacade.deleteBand(4L);

            Mockito.verify(bandService, Mockito.times(1)).deleteBand(4L);
        }
    }
}