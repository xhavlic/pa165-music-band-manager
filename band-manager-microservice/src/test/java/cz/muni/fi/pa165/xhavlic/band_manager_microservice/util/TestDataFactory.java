package cz.muni.fi.pa165.xhavlic.band_manager_microservice.util;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.AlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.DetailedBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputAlbumDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputBandDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputBandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputSongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.SongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Song;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TestDataFactory {
    public static Album album = getAlbumFactory();
    public static Album album2 = getAlbumFactory2();

    public static List<Album> albums = List.of(album, album2);

    public static Band band = getBandFactory();

    public static Band bandWithNoAlbums = getBandFactoryWithNoAlbums();

    public static Song song = getSongFactory();

    public static List<Band> bands = List.of(band, bandWithNoAlbums);

    public static InputAlbumDto inputAlbumDto = getInputAlbumDtoFactory();
    public static AlbumDto albumDto = getAlbumDtoFactory();

    public static List<AlbumDto> albumDtos = List.of(albumDto);

    public static InputSongDto inputSongDto = getInputSongFactory();
    public static SongDto songDto = getSongDtoFactory();

    public static DetailedBandDto detailedBandDto = getDetailedBandDtoFactory();

    public static InputBandDto inputBandDto = getInputBandDtoFactory();
    public static BandDto bandDto = getBandDtoFactory();

    public static InputBandMemberDto inputBandMemberDto = getInputBandMemberFactory();
    public static BandMemberDto bandMemberDto = getBandMemberDtoFactory();

    public static List<BandDto> bandsDtos = List.of(bandDto);

    private static InputBandDto getInputBandDtoFactory() {
        return new InputBandDto("www.x.com", "genre", "name");
    }

    private static BandDto getBandDtoFactory() {
        BandDto bandDto = new BandDto();
        bandDto.setId(1L);
        bandDto.setName("Test Band");
        return bandDto;
    }

    private static DetailedBandDto getDetailedBandDtoFactory() {
        DetailedBandDto detailedBandDto = new DetailedBandDto();
        detailedBandDto.setId(3L);
        detailedBandDto.setName("Test Band");
        detailedBandDto.setAlbums(new ArrayList<>());
        return detailedBandDto;
    }

    private static InputSongDto getInputSongFactory() {
        return new InputSongDto(1, "song");
    }

    private static SongDto getSongDtoFactory() {
        SongDto songDto = new SongDto();
        songDto.setId(1L);
        songDto.setName("Test Song");
        return songDto;
    }

    private static InputAlbumDto getInputAlbumDtoFactory() {
        return new InputAlbumDto("input", new ArrayList<>());
    }

    private static AlbumDto getAlbumDtoFactory() {
        AlbumDto albumDto = new AlbumDto();
        albumDto.setId(1L);
        albumDto.setName("Test Album");
        albumDto.setSongs(new ArrayList<>());
        return albumDto;
    }

    private static Song getSongFactory() {
        Song song = new Song();
        song.setId(1L);
        song.setName("Test Song");
        return song;
    }

    private static Band getBandFactoryWithNoAlbums() {
        Band band = new Band();
        band.setId(2L);
        band.setName("Test Band 2");
        band.setAlbums(new ArrayList<>());
        return band;
    }

    private static Band getBandFactory() {
        Band band = new Band();
        band.setId(1L);
        band.setName("Test Band");
        band.setAlbums(new ArrayList<>(albums));
        return band;
    }

    private static Album getAlbumFactory() {
        Album album = new Album();
        album.setId(1L);
        album.setName("Test Album");
        album.setSongs(new ArrayList<>());
        return album;
    }

    private static Album getAlbumFactory2() {
        Album album = new Album();
        album.setId(2L);
        album.setName("Test Album 2");
        album.setSongs(new ArrayList<>());
        return album;
    }

    private static InputBandMemberDto getInputBandMemberFactory() {
        return new InputBandMemberDto("name", "surname");
    }

    private static BandMemberDto getBandMemberDtoFactory() {
        BandMemberDto bandMemberDto = new BandMemberDto();
        bandMemberDto.setId(1L);
        bandMemberDto.setFirstName("John");
        bandMemberDto.setLastName("Lennon");
        return bandMemberDto;
    }
}
