package cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.SongDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper.SongMapper;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.service.SongService;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.util.TestDataFactory;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class SongFacadeTest {

    @InjectMocks
    private SongFacade songFacade;

    @Mock
    private SongMapper songMapper;

    @Mock
    private SongService songService;

    @Nested
    class addSongToAlbum {
        @Test
        void shouldReturnOfAddedSong() {
            Mockito.when(songService.addSongToAlbum(1L, TestDataFactory.song)).thenReturn(TestDataFactory.song);
            Mockito.when(songMapper.mapFromDto(any())).thenReturn(TestDataFactory.song);
            Mockito.when(songMapper.mapToDto(TestDataFactory.song)).thenReturn(TestDataFactory.songDto);

            assertEquals(TestDataFactory.songDto, songFacade.addSongToAlbum(1L, TestDataFactory.inputSongDto));
        }

    }

    @Nested
    class getSongById {
        @Test
        void shouldReturnSong() {
            Mockito.when(songMapper.mapToDto(any())).thenReturn(TestDataFactory.songDto);
            Mockito.when(songService.getSongById(1L)).thenReturn(TestDataFactory.song);

            SongDto songById = songFacade.getSongById(1L);

            assertEquals(TestDataFactory.songDto, songById);
        }
    }

    @Nested
    class updateSong {
        @Test
        void shouldReturnUpdatedSong() {
            Mockito.when(songMapper.mapFromDto(any())).thenReturn(TestDataFactory.song);
            Mockito.when(songMapper.mapToDto(any())).thenReturn(TestDataFactory.songDto);
            Mockito.when(songService.updateSong(1L, TestDataFactory.song)).thenReturn(TestDataFactory.song);

            SongDto updatedSong = songFacade.updateSong(1L, TestDataFactory.inputSongDto);

            assertEquals(TestDataFactory.songDto, updatedSong);
        }
    }

}