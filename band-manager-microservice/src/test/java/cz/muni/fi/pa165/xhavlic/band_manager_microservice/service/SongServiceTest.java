package cz.muni.fi.pa165.xhavlic.band_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Song;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.SongRepository;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.util.TestDataFactory;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class SongServiceTest {

    @Mock
    private SongRepository songRepository;
    @Mock
    private AlbumService albumService;

    @InjectMocks
    private SongService songService;

    @Nested
    class addSongToAlbum {
        @Test
        void shouldReturnAddedSong() {
            Mockito.when(songRepository.save(TestDataFactory.song)).thenReturn(TestDataFactory.song);
            Mockito.when(albumService.getAlbumById(1L)).thenReturn(TestDataFactory.album);

            var addedSong = songService.addSongToAlbum(1L, TestDataFactory.song);

            assertEquals(addedSong, TestDataFactory.song);
            assertEquals(albumService.getAlbumById(1L).getSongs(), TestDataFactory.album.getSongs());

        }

        @Test
        void throwExceptionWhenAlbumDoesNotExist() {
            Mockito.when(songRepository.save(TestDataFactory.song)).thenReturn(TestDataFactory.song);
            Mockito.when(albumService.getAlbumById(1L)).thenThrow(ResponseStatusException.class);

            assertThrows(ResponseStatusException.class, () -> songService.addSongToAlbum(1L, TestDataFactory.song));
        }
    }

    @Nested
    class getSongById {
        @Test
        void shouldReturnSong() {
            Mockito.when(songRepository.findSongById(1L)).thenReturn(Optional.ofNullable(TestDataFactory.song));

            Song foundSong = songService.getSongById(1L);

            assertEquals(foundSong, TestDataFactory.song);
        }

        @Test
        void shouldThrowExceptionWhenSongDoesNotExist() {
            Mockito.when(songRepository.findSongById(1L)).thenReturn(Optional.empty());
            assertThrows(ResponseStatusException.class, () -> songService.getSongById(1L));
        }

        @Test
        void shouldThrowExceptionWhenIdExists() {
            Mockito.when(songRepository.findSongById(1L)).thenReturn(Optional.empty());
            assertThrows(ResponseStatusException.class, () -> songService.getSongById(1L));
        }
    }

    @Nested
    class updateSong {
        @Test
        void shouldReturnUpdatedSong() {
            var existingSong = new Song();
            existingSong.setId(1L);
            existingSong.setName("Old Name");
            existingSong.setDuration(300);

            var updatedSongDetails = new Song();
            updatedSongDetails.setName("New Name");
            updatedSongDetails.setDuration(350);

            Mockito.when(songRepository.updateSong(1L, 350, "New Name")).thenReturn(1);

            var updatedSong = new Song();
            updatedSong.setId(1L);
            updatedSong.setName("New Name");
            updatedSong.setDuration(350);
            Mockito.when(songRepository.findSongById(1L)).thenReturn(Optional.of(updatedSong));

            Song result = songService.updateSong(1L, updatedSongDetails);

            assertThat(result.getName()).isEqualTo("New Name");
            assertThat(result.getDuration()).isEqualTo(350);
            Mockito.verify(songRepository).updateSong(1L, 350, "New Name");
            Mockito.verify(songRepository).findSongById(1L);
        }

        @Test
        void shouldThrowExceptionWhenSongDoesNotExist() {
            var song = TestDataFactory.song;
            Mockito.when(songRepository.updateSong(1L, song.getDuration(),song.getName() )).thenReturn(0);

            assertThrows(ResponseStatusException.class, () -> songService.updateSong(1L, TestDataFactory.song));
        }
    }

}