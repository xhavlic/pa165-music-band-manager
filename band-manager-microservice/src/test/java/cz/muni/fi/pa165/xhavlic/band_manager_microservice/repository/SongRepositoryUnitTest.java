package cz.muni.fi.pa165.xhavlic.band_manager_microservice.repository;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Song;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.SongRepository;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@ComponentScan(basePackageClasses = SongRepository.class)
@ActiveProfiles("test")
public class SongRepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private SongRepository songRepository;

    @Test
    public void testFindSongByIdWhenExists() {
        Song song = new Song();
        song.setName("Comfortably Numb");
        song.setDuration(378);
        entityManager.persist(song);
        entityManager.flush();

        Song found = songRepository.findSongById(song.getId()).orElse(null);
        assertThat(found).isNotNull();
        assertThat(found.getName()).isEqualTo("Comfortably Numb");
    }
    @Test
    public void testSaveSong() {
        Song song = new Song();
        song.setName("Wish You Were Here");
        song.setDuration(334);
        Song savedSong = songRepository.save(song);

        Song found = entityManager.find(Song.class, savedSong.getId());
        assertThat(found).isEqualTo(savedSong);
    }
    @Test
    public void testUpdateSong() {
        Song song = new Song();
        song.setName("Time");
        song.setDuration(412);
        entityManager.persist(song);
        entityManager.flush();

        songRepository.updateSong(song.getId(), 413, "Time (Remastered)");
        entityManager.clear();

        Song updatedSong = entityManager.find(Song.class, song.getId());
        assertThat(updatedSong.getDuration()).isEqualTo(413);
        assertThat(updatedSong.getName()).isEqualTo("Time (Remastered)");
    }
    @Test
    @Transactional
    public void testDeleteSongById() {
        Song song = new Song();
        song.setName("High Hopes");
        song.setDuration(482);
        song = entityManager.persistFlushFind(song);

        songRepository.deleteById(song.getId());

        boolean exists = songRepository.existsById(song.getId());
        assertThat(exists).isFalse();
    }
}