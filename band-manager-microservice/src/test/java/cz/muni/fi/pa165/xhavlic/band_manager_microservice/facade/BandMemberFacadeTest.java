package cz.muni.fi.pa165.xhavlic.band_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.BandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.api.InputBandMemberDto;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.BandMember;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.mapper.BandMemberMapper;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.service.BandMemberService;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class BandMemberFacadeTest {

    @Mock
    private BandMemberService bandMemberService;

    @Mock
    private BandMemberMapper bandMemberMapper;

    @InjectMocks
    private BandMemberFacade bandMemberFacade;

    @Test
    void getBandMemberByIdShouldReturnBandMemberDto() {
        long id = TestDataFactory.bandMemberDto.getId();
        BandMember bandMember = new BandMember(1L, "John", "Lennon");
        BandMemberDto expectedDto = TestDataFactory.bandMemberDto;

        when(bandMemberService.getBandMemberById(id)).thenReturn(bandMember);
        when(bandMemberMapper.mapToDto(any())).thenReturn(expectedDto);

        BandMemberDto resultDto = bandMemberFacade.getBandMemberById(id);

        assertThat(resultDto).isEqualTo(expectedDto);
        verify(bandMemberService).getBandMemberById(id);
        verify(bandMemberMapper).mapToDto(any());
    }

    @Test
    void updateBandMemberShouldReturnUpdatedBandMemberDto() {
        long id = TestDataFactory.bandMemberDto.getId();
        BandMemberDto updateDto = new BandMemberDto(id, "Updated John", "Updated Lennon");
        BandMember updatedBandMember = new BandMember(id, "Updated John", "Updated Lennon");
        var inputBandMember = TestDataFactory.inputBandMemberDto;

        when(bandMemberService.updateBandMember(eq(id), any(BandMember.class))).thenReturn(updatedBandMember);
        when(bandMemberMapper.mapToDto(any(BandMember.class))).thenReturn(updateDto);
        when(bandMemberMapper.mapFromDto(any(InputBandMemberDto.class))).thenReturn(updatedBandMember);

        BandMemberDto resultDto = bandMemberFacade.updateBandMember(id, inputBandMember );

        assertThat(resultDto).isEqualTo(updateDto);
        verify(bandMemberService).updateBandMember(eq(id), any(BandMember.class));
        verify(bandMemberMapper).mapToDto(any(BandMember.class));
        verify(bandMemberMapper).mapFromDto(any(InputBandMemberDto.class));
    }

    @Test
    void createNewBandMemberShouldReturnNewBandMemberId() {
        BandMemberDto newMemberDto = new BandMemberDto(0, "New First Name", "New Last Name");
        BandMember newBandMember = new BandMember(0L, "New First Name", "New Last Name");
        var inputBandMember = TestDataFactory.inputBandMemberDto;

        when(bandMemberService.createNewBandMember(any(BandMember.class))).thenReturn(newBandMember);
        when(bandMemberMapper.mapFromDto(any(InputBandMemberDto.class))).thenReturn(newBandMember);
        when(bandMemberMapper.mapToDto(newBandMember)).thenReturn(newMemberDto);

        var createdBand = bandMemberFacade.createNewBandMember(inputBandMember);

        assertThat(createdBand).isEqualTo(newMemberDto);
        verify(bandMemberService).createNewBandMember(any(BandMember.class));
        verify(bandMemberMapper).mapFromDto(any(InputBandMemberDto.class));
    }

    @Test
    void addBandMemberToBandShouldReturnMember() {
        long bandId = 1L;
        BandMemberDto bandMemberDto = TestDataFactory.bandMemberDto;
        BandMember bandMember = new BandMember(bandMemberDto.getId(), bandMemberDto.getFirstName(), bandMemberDto.getLastName());
        var inputBandMember = TestDataFactory.inputBandMemberDto;

        when(bandMemberService.addBandMemberToBand(eq(bandId), any(BandMember.class))).thenReturn(bandMember);
        when(bandMemberMapper.mapFromDto(any(InputBandMemberDto.class))).thenReturn(bandMember);
        when(bandMemberMapper.mapToDto(bandMember)).thenReturn(bandMemberDto);

        var createdBandMember = bandMemberFacade.addBandMemberToBand(bandId, inputBandMember);

        assertThat(createdBandMember).isEqualTo(bandMemberDto);
        verify(bandMemberService).addBandMemberToBand(eq(bandId), any(BandMember.class));
        verify(bandMemberMapper).mapFromDto(any(InputBandMemberDto.class));
    }

    @Test
    void deleteBandMemberShouldCallServiceMethod() {
        long id = TestDataFactory.bandMemberDto.getId();

        doNothing().when(bandMemberService).deleteBandMember(id);

        bandMemberFacade.deleteBandMember(id);

        verify(bandMemberService).deleteBandMember(id);
    }
}
