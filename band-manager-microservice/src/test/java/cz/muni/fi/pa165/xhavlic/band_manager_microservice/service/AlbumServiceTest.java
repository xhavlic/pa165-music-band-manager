package cz.muni.fi.pa165.xhavlic.band_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Album;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.model.Band;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.data.repository.interfaces.AlbumRepository;
import cz.muni.fi.pa165.xhavlic.band_manager_microservice.util.TestDataFactory;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class AlbumServiceTest {

    @Mock
    private AlbumRepository albumRepository;

    @InjectMocks
    private AlbumService albumService;

    @Mock
    private BandService bandService;

    @Nested
    class getAlbumById {
        @Test
        void shouldReturnAlbum() {
            when(albumRepository.findAlbumById(1L)).thenReturn(Optional.ofNullable(TestDataFactory.album));

            Album found = albumService.getAlbumById(1L);

            assertThat(found).isEqualTo(TestDataFactory.album);
        }

        @Test
        void shouldThrowExceptionWhenAlbumDoesNotExist() {
            when(albumRepository.findAlbumById(1L)).thenReturn(Optional.empty());
            assertThrows(ResponseStatusException.class, () -> albumService.getAlbumById(1L));
        }
    }

    @Nested
    class getAllAlbumsForBandId {
        @Test
        void shouldReturnAlbums() {
            when(bandService.getBandById(1L)).thenReturn(TestDataFactory.band);

            List<Album> foundAlbums = albumService.getAllAlbumsForBandId(1L);

            assertThat(foundAlbums).isEqualTo(TestDataFactory.albums);
        }

        @Test
        void shouldReturnEmptyListWhenBandHasNoAlbums() {
            when(bandService.getBandById(1L)).thenReturn(TestDataFactory.bandWithNoAlbums);

            List<Album> foundAlbums = albumService.getAllAlbumsForBandId(1L);

            assertThat(foundAlbums).isEqualTo(List.of());
        }
    }

    @Nested
    class addAlbumToBand {
        @Test
        void shouldReturnAlbumId() {
        }

        @Test
        void shouldThrowExceptionWhenBandDoesNotExist() {
        }
    }

    @Nested
    class updateAlbum {
        @Test
        void shouldReturnUpdatedAlbum() {
            var expected = TestDataFactory.album;
            expected.setName("expectedAlbum");
            when(albumRepository.findAlbumById(1L)).thenReturn(Optional.of(TestDataFactory.album));

            Album updated = albumService.updateAlbum(1L, expected);

            assertThat(updated).isEqualTo(expected);
        }

        @Test
        void shouldThrowExceptionWhenAlbumDoesNotExist() {
            when(albumRepository.findAlbumById(1L)).thenReturn(Optional.empty());

            assertThrows(ResponseStatusException.class, () -> albumService.updateAlbum(1L, TestDataFactory.album));
        }
    }

    @Nested
    class SearchAlbums {
        @Test
        void shouldReturnAlbumsFilteredByBandName() {
            String bandName = "Test Band";
            Band band = TestDataFactory.band;
            when(bandService.getBandByName(bandName)).thenReturn(band);

            List<Album> expectedAlbums = band.getAlbums();
            List<Album> foundAlbums = albumService.searchAlbums(null, bandName, null);

            assertThat(foundAlbums).isExactlyInstanceOf(expectedAlbums.getClass());
        }

        @Test
        void shouldReturnAlbumsFilteredByAlbumName() {
            String albumName = "Test Album";
            List<Album> albums = TestDataFactory.albums;
            when(albumRepository.findAlbumsByName(albumName)).thenReturn(albums.stream().toList());

            List<Album> foundAlbums = albumService.searchAlbums(null, null, albumName);

            assertThat(foundAlbums).isEqualTo(albums);
        }

        @Test
        void shouldReturnEmptyListWhenNoMatchingAlbums() {
            String genre = "Pop";
            when(bandService.getBandsByGenre(genre)).thenReturn(Collections.emptyList());
            List<Album> foundAlbums = albumService.searchAlbums(genre, null, null);
            assertTrue(foundAlbums.isEmpty());
        }

        @Test
        void shouldThrowIllegalArgumentExceptionWhenNoFiltersProvided() {
            assertThrows(IllegalArgumentException.class, () -> albumService.searchAlbums(null, null, null));
        }
    }
}