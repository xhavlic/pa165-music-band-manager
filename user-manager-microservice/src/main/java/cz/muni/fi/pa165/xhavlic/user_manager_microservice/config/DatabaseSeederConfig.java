package cz.muni.fi.pa165.xhavlic.user_manager_microservice.config;

import com.github.javafaker.Faker;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.MusicStyle;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.User;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.repository.interfaces.MusicStyleRepository;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.repository.interfaces.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Configuration
public class DatabaseSeederConfig {

    private static final Faker faker = new Faker();
    private static final int numberOfUsers = faker.number().numberBetween(30, 60);

    @Bean
    @Transactional
    public CommandLineRunner databaseSeeder(UserRepository userRepository, MusicStyleRepository musicStyleRepository) {
        return args -> {
            String seedData = System.getProperty("seed.data", "false");
            String clearDb = System.getProperty("clear.db", "false");
            System.out.println("Seed data property: " + seedData);
            System.out.println("Clear DB property: " + clearDb);

            // Optionally clear the database
            if (Boolean.parseBoolean(clearDb)) {
                System.out.println("Clearing the database...");
                userRepository.deleteAllInBatch();
                musicStyleRepository.deleteAllInBatch();
            }
            // Seed the database with random data
            if (Boolean.parseBoolean(seedData)) {

                List<User> users = createUsers();

                userRepository.saveAll(users);
                System.out.println("Database has been seeded.");
            }
        };
    }

    private List<User> createUsers() {
        return faker.lorem().words(numberOfUsers).stream()
                .map(name -> {
                    User user = new User();
                    user.setUsername(faker.name().username());
                    user.setEmail(faker.internet().emailAddress());
                    user.setPassword(faker.internet().password());
                    user.setMusicStyles((generateMusicStyles()));
                    return user;
                })
                .collect(Collectors.toList());
    }

    private List<MusicStyle> generateMusicStyles() {
        Set<String> uniqueGenres = new HashSet<>();
        List<MusicStyle> musicStyles = new ArrayList<>();
        Faker faker = new Faker();

        while (uniqueGenres.size() < faker.number().numberBetween(1, 4)) {
            String genre = faker.music().genre();
            if (uniqueGenres.add(genre)) {
                MusicStyle musicStyle = new MusicStyle();
                musicStyle.setName(genre);
                musicStyles.add(musicStyle);
            }
        }

        return musicStyles;
    }
}
