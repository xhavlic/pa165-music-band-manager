package cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.repository.interfaces;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.MusicStyle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MusicStyleRepository extends JpaRepository<MusicStyle, Long> {
    Optional<MusicStyle> findByName(String name);
}