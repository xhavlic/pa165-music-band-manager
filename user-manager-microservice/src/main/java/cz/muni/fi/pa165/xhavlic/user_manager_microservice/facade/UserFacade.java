package cz.muni.fi.pa165.xhavlic.user_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserLoginDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.exceptions.UserExceptions;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.mapper.UserMapper;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserFacade {

    private final UserService userService;
    private final UserMapper userMapper;
    @Autowired
    public UserFacade(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }
    public UserDto createUser(UserDto userDto) {
        try {
            var user = userMapper.mapFromDto(userDto);
            user = userService.createUser(user);
            return userMapper.mapToDto(user);
        } catch (UserExceptions.UserCreationException e) {
            throw new RuntimeException("Error creating user: " + e.getMessage());
        }
    }
    public UserDto updateUser(Long id, UserDto userDto) {
        var updatedUser = userService.updateUser(id, userMapper.mapFromDto(userDto));
        return userMapper.mapToDto(updatedUser);
    }
    public boolean loginUser(UserLoginDto userLoginDto) {
        return userService.validateCredentials(userLoginDto);
    }
    public List<UserDto> findAllUsers() {
        return userMapper.mapToList(userService.findAll());
    }
    public UserDto deleteUser(Long id) {
        return userMapper.mapToDto(userService.deleteUser(id));
    }

}
