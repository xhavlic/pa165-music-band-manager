package cz.muni.fi.pa165.xhavlic.user_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserLoginDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.MusicStyle;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.User;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.repository.interfaces.MusicStyleRepository;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.repository.interfaces.UserRepository;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.exceptions.UserExceptions;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.config.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final MusicStyleRepository musicStyleRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, MusicStyleRepository musicStyleRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.musicStyleRepository = musicStyleRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Transactional
    public User createUser(User user) throws UserExceptions.UserCreationException {
        if (user.getMusicStyles() != null && !user.getMusicStyles().isEmpty()) {
            user.setMusicStyles(user.getMusicStyles().stream()
                    .map(style -> findOrCreateMusicStyle(style.getName()))
                    .collect(Collectors.toList()));
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        try {
            if (userRepository.findByUsername(user.getUsername()).isPresent()) {
                throw new UserExceptions.UserCreationException("Username already exists: " + user.getUsername());
            }
            if (userRepository.findByEmail(user.getEmail()).isPresent()) {
                throw new UserExceptions.UserCreationException("Email already exists: " + user.getEmail());
            }
            return userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new UserExceptions.UserCreationException("Failed to create a user due to a data integrity issue.");
        }
    }
    @Transactional
    protected MusicStyle findOrCreateMusicStyle(String name) {
        return musicStyleRepository.findByName(name)
                .orElseGet(() -> musicStyleRepository.save(new MusicStyle(null, name)));
    }
    @Transactional
    public User updateUser(Long id, User updatedUser) {
        User existingUser = userRepository.findById(id)
                .orElseThrow(() -> new UserExceptions.UserNotFoundException("User with ID " + id + " not found."));
        updateUserData(existingUser, updatedUser);
        return userRepository.save(existingUser);
    }
    @Transactional
    protected void updateUserData(User existingUser, User updatedUser) {
        if (updatedUser.getUsername() != null) {
            existingUser.setUsername(updatedUser.getUsername());
        }
        if (updatedUser.getEmail() != null) {
            existingUser.setEmail(updatedUser.getEmail());
        }
        if (updatedUser.getPassword() != null) {
            existingUser.setPassword(updatedUser.getPassword());
        }
        if (updatedUser.getMusicStyles() != null) {
            List<MusicStyle> updatedStyles = updatedUser.getMusicStyles().stream()
                    .map(style -> findOrCreateMusicStyle(style.getName()))
                    .collect(Collectors.toList());
            existingUser.setMusicStyles(updatedStyles);
        }
    }
    @Transactional
    public User deleteUser(long id) {
        User user = userRepository.getUserById(id).orElseThrow(
                () -> new UserExceptions.UserNotFoundException("User with ID " + id + " not found.")
        );
        int rowsAffected = userRepository.deleteUserById(id);
        if (rowsAffected < 1) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "User with id: " + id + " could not be deleted");
        }
        return user;
    }
    @Transactional
    public boolean validateCredentials(UserLoginDto userLoginDto) {
        User user = userRepository.findByUsername(userLoginDto.getUsername())
                .orElseThrow(() -> new UserExceptions.UserNotFoundException("User with username " + userLoginDto.getUsername() + " not found."));
        return passwordEncoder.matches(userLoginDto.getPassword(), user.getPassword());
    }
    @Transactional
    public List<User> findAll() {
        List<User> users = userRepository.findAll();
        users.forEach(user -> user.getMusicStyles().size());
        return users;
    }
}
