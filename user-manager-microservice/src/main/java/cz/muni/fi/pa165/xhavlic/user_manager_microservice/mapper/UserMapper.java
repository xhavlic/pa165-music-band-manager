package cz.muni.fi.pa165.xhavlic.user_manager_microservice.mapper;

import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.MusicStyle;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "musicStyles", source = "musicStyles", qualifiedByName = "musicStyleListToStringList")
    UserDto mapToDto(User user);

    @Mapping(target = "musicStyles", source = "musicStyles", qualifiedByName = "stringListToMusicStyleList")
    User mapFromDto(UserDto userDto);

    List<UserDto> mapToList(List<User> user);

    @Named("musicStyleListToStringList")
    default List<String> mapMusicStylesToStrings(List<MusicStyle> musicStyles) {
        if (musicStyles == null) {
            return null;
        }
        return musicStyles.stream().map(MusicStyle::getName).collect(Collectors.toList());
    }

    @Named("stringListToMusicStyleList")
    default List<MusicStyle> mapStringsToMusicStyles(List<String> strings) {
        if (strings == null) {
            return null;
        }
        return strings.stream().map(name -> new MusicStyle(null, name)).collect(Collectors.toList());
    }
}