package cz.muni.fi.pa165.xhavlic.user_manager_microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserManagerApp {
    public static void main(String[] args) {
        SpringApplication.run(UserManagerApp.class, args);
    }
}