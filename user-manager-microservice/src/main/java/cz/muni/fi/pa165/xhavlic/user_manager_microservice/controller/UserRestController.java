package cz.muni.fi.pa165.xhavlic.user_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserLoginDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.exceptions.UserExceptions;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.facade.UserFacade;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.oauth2.UserAuthentication;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private final UserFacade userFacade;

    @Autowired
    public UserRestController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Operation(summary = "Registers a new user", description = "Creates a new user with the provided information.")
    @SecurityRequirement(name = UserAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User created successfully",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))),
            @ApiResponse(responseCode = "400", description = "Invalid user data supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
    })
    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody UserDto userDto) {
        try {
            UserDto createdUser = userFacade.createUser(userDto);
            return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
        } catch (UserExceptions.UserCreationException e) {
            return ResponseEntity.badRequest().body(Collections.singletonMap("message", e.getMessage()));
        }
    }

    @Operation(summary = "Logs in a user", description = "Logs in the user with provided username and password.")
    @SecurityRequirement(name = UserAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User logged in successfully"),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
    })
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserLoginDto loginUser) {
        boolean loginSuccess = userFacade.loginUser(loginUser);
        if (loginSuccess) {
            return ResponseEntity.ok().body("User logged in successfully!");
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Login failed");
        }
    }

    @Operation(summary = "Updates a user", description = "Updates the user's information for the given ID.")
    @SecurityRequirement(name = UserAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated successfully",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))),
            @ApiResponse(responseCode = "400", description = "Invalid id or body supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable Long id, @Valid @RequestBody UserDto userDto) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(userFacade.updateUser(id, userDto));
    }

    @Operation(summary = "Get all Users", description = "Get list of all user")
    @SecurityRequirement(name = UserAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the users",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content())})
    @GetMapping
    public ResponseEntity<List<UserDto>> listAllUsers() {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(userFacade.findAllUsers());
    }

    @Operation(summary = "Deletes a user", description = "Deletes the user with the provided ID.")
    @SecurityRequirement(name = UserAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User deleted successfully", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_1", content = @Content()),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) {
        if (id == null || id <= 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid ID supplied");
        }
        try {
            UserDto userDto = userFacade.deleteUser(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(userDto);
        } catch (UserExceptions.UserNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}