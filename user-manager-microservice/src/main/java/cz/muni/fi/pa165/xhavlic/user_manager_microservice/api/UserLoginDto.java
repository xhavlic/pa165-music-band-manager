package cz.muni.fi.pa165.xhavlic.user_manager_microservice.api;

import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("UserLogin")
public class UserLoginDto {
    private long id;
    @Schema(example = "username", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;
    @Schema(example = "password", requiredMode = Schema.RequiredMode.REQUIRED)
    private String password;

    public UserLoginDto(String username, String pass) {
        this.username = username;
        this.password = pass;
    }
}
