package cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.repository.interfaces;

import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Repository
public interface  UserRepository extends JpaRepository<User, Long> {
    @Transactional
    Optional<User> findByUsername(String username);
    @Transactional
    Optional<User> findByEmail(String username);
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Transactional
    int deleteUserById(long id);
    @Transactional
    Optional<User> getUserById(long id);
}
