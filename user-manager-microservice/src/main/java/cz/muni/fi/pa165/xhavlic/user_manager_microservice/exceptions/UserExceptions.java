package cz.muni.fi.pa165.xhavlic.user_manager_microservice.exceptions;

public class UserExceptions {
    public static class UserNotFoundException extends RuntimeException {
        public UserNotFoundException(String message) {
            super(message);
        }
    }

    public static class UserCreationException extends RuntimeException {
        public UserCreationException(String message) {
            super(message);
        }
    }
}
