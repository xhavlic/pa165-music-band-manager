package cz.muni.fi.pa165.xhavlic.user_manager_microservice.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("User")
public class UserDto {
    private long id;
    @Schema(example = "John", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;
    @Schema(example = "john@gmail.com", requiredMode = Schema.RequiredMode.REQUIRED)
    private String email;
    @Schema(example = "password", requiredMode = Schema.RequiredMode.REQUIRED)
    private String password;
    @ArraySchema(arraySchema = @Schema(
            example = "[\"rock\", \"metal\"]",
            description = "List of music styles associated with the user",
            requiredMode = Schema.RequiredMode.NOT_REQUIRED
    ))
    @JsonProperty("musicStyles")
    private List<String> musicStyles;

    public UserDto(String username, String email, String pass, List<String> list) {
        this.username = username;
        this.email = email;
        this.password = pass;
        this.musicStyles = list;
    }
}
