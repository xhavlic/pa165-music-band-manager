package cz.muni.fi.pa165.xhavlic.user_manager_microservice.facade;

import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserLoginDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.MusicStyle;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.User;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.exceptions.UserExceptions;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.mapper.UserMapper;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */
@ExtendWith(MockitoExtension.class)
class UserFacadeTest {

    @Mock
    public UserService userService;

    @Mock
    public UserMapper userMapper;

    @InjectMocks
    public UserFacade userFacade;

    @Test
    void createUser_everyThingFine_userCreated() {
        var expectedUserDto = new UserDto("name", "email", "pass", Arrays.asList("rock", "metal"));
        List<MusicStyle> styles = Arrays.asList(new MusicStyle(1L, "rock"), new MusicStyle(2L, "metal"));
        var user = new User(1L, "name", "email", "pass", styles);
        when(userMapper.mapFromDto(expectedUserDto)).thenReturn(user);
        when(userMapper.mapToDto(user)).thenReturn(expectedUserDto);
        when(userService.createUser(user)).thenReturn(user);

        var result = userFacade.createUser(expectedUserDto);

        assertThat(result).isEqualTo(expectedUserDto);
        verify(userService).createUser(user);
        verify(userMapper).mapFromDto(expectedUserDto);
        verify(userMapper).mapToDto(user);

    }

    @Test
    void update_everythingFine_userUpdated() {
        var expectedUserDto = new UserDto("name", "email", "pass", Arrays.asList("rock", "metal"));
        List<MusicStyle> styles = Arrays.asList(new MusicStyle(1L, "rock"), new MusicStyle(2L, "metal"));
        var user = new User(1L, "name", "email", "pass", styles);
        var id = 1L;
        when(userMapper.mapFromDto(expectedUserDto)).thenReturn(user);
        when(userMapper.mapToDto(user)).thenReturn(expectedUserDto);
        when(userService.updateUser(id, user)).thenReturn(user);


        var result = userFacade.updateUser(id, expectedUserDto);

        assertThat(result).isEqualTo(expectedUserDto);
        verify(userService).updateUser(id, user);
        verify(userMapper).mapFromDto(expectedUserDto);
        verify(userMapper).mapToDto(user);
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void loginUser(boolean valid) {
        var userLoginDto = new UserLoginDto("email", "pass");

        when(userService.validateCredentials(userLoginDto)).thenReturn(valid);
        var result = userFacade.loginUser(userLoginDto);

        assertThat(result).isEqualTo(valid);
        verify(userService).validateCredentials(userLoginDto);
    }

    @Test
    void findAllUsers() {

        var expectedUserDto = new UserDto("name", "email", "pass", Arrays.asList("rock", "metal"));
        List<MusicStyle> styles = Arrays.asList(new MusicStyle(1L, "rock"), new MusicStyle(2L, "metal"));
        var user = new User(1L, "name", "email", "pass",  styles);
        var users = List.of(user);
        var expectedUserDtos = List.of(expectedUserDto);
        when(userMapper.mapToList(users)).thenReturn(expectedUserDtos);
        when(userService.findAll()).thenReturn(users);

        var result = userFacade.findAllUsers();

        assertThat(result).isEqualTo(expectedUserDtos);
        verify(userService).findAll();
        verify(userMapper).mapToList(users);
    }
    @Test
    void deleteUser_userExists_returnUserDto() {
        long id = 1L;
        List<MusicStyle> styles = Arrays.asList(new MusicStyle(1L, "rock"), new MusicStyle(2L, "metal"));
        User user = new User(id, "JohnDoe", "john.doe@example.com", "password123", styles);
        UserDto expectedUserDto = new UserDto("JohnDoe", "john.doe@example.com", "password123", Arrays.asList("rock", "metal"));

        when(userService.deleteUser(id)).thenReturn(user);
        when(userMapper.mapToDto(user)).thenReturn(expectedUserDto);

        UserDto result = userFacade.deleteUser(id);

        assertThat(result).isEqualTo(expectedUserDto);
        verify(userService).deleteUser(id);
        verify(userMapper).mapToDto(user);
    }

    @Test
    void createUser_Success_ReturnsDto() {
        UserDto userDto = new UserDto("newuser", "newemail@example.com", "password", Collections.emptyList());
        User user = new User(null, "newuser", "newemail@example.com", "password", Collections.emptyList());

        when(userMapper.mapFromDto(any(UserDto.class))).thenReturn(user);
        when(userService.createUser(any(User.class))).thenReturn(user);
        when(userMapper.mapToDto(any(User.class))).thenReturn(userDto);

        UserDto result = userFacade.createUser(userDto);

        assertNotNull(result);
        assertEquals("newuser", result.getUsername());
    }

    @Test
    void createUser_ThrowsUserCreationException_TranslatesException() {
        UserDto userDto = new UserDto("newuser", "newemail@example.com", "password", Collections.emptyList());
        when(userMapper.mapFromDto(any(UserDto.class))).thenReturn(new User());
        when(userService.createUser(any(User.class))).thenThrow(new UserExceptions.UserCreationException("Failed to create user"));

        Exception exception = assertThrows(RuntimeException.class, () -> userFacade.createUser(userDto));

        assertEquals("Error creating user: Failed to create user", exception.getMessage());
    }
}