package cz.muni.fi.pa165.xhavlic.user_manager_microservice.controller;

import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserLoginDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.facade.UserFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */
@ExtendWith(MockitoExtension.class)
class UserRestControllerTest {

    @InjectMocks
    public UserRestController userRestController;

    @Mock
    public UserFacade userFacade;


    @Test
    void registerUser_everyThingFine_userRegistered() {
        var expectedUserDto = new UserDto("name", "email", "pass", Arrays.asList("rock", "metal"));
        when(userFacade.createUser(expectedUserDto)).thenReturn(expectedUserDto);

        var result = userRestController.registerUser(expectedUserDto);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(result.getBody()).isEqualTo(expectedUserDto);
        verify(userFacade).createUser(expectedUserDto);
    }

    @Test
    void login_loginSuccess_CorrectMessage() {
        var userLoginDto = new UserLoginDto("email", "pass");
        when(userFacade.loginUser(userLoginDto)).thenReturn(true);

        var result = userRestController.login(userLoginDto);

        verify(userFacade).loginUser(userLoginDto);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo("User logged in successfully!");
    }

    @Test
    void login_loginNotSuccess_UnAuthorizedMessage() {
        var userLoginDto = new UserLoginDto("email", "pass");
        when(userFacade.loginUser(userLoginDto)).thenReturn(false);

        var result = userRestController.login(userLoginDto);

        verify(userFacade).loginUser(userLoginDto);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(result.getBody()).isEqualTo("Login failed");
    }

    @Test
    void updateUser() {
        var expectedUserDto = new UserDto("name", "email", "pass", Arrays.asList("rock", "metal"));
        var id = 1L;
        when(userFacade.updateUser(id, expectedUserDto)).thenReturn(expectedUserDto);

        var result = userRestController.updateUser(id, expectedUserDto);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(expectedUserDto);
        verify(userFacade).updateUser(id, expectedUserDto);
    }

    @Test
    void listAllBands() {
        var expectedUserDto = new UserDto("name", "email", "pass", Arrays.asList("rock", "metal"));
        var expectedUserDtos = List.of(expectedUserDto);
        when(userFacade.findAllUsers()).thenReturn(expectedUserDtos);

        var result = userRestController.listAllUsers();

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(expectedUserDtos);
        verify(userFacade).findAllUsers();
    }
    @Test
    void deleteUser_userExists_returnNoContent() {
        Long id = 1L;
        UserDto expectedUserDto = new UserDto("JohnDoe", "john.doe@example.com", "password123", Arrays.asList("rock", "metal"));
        when(userFacade.deleteUser(id)).thenReturn(expectedUserDto);

        var result = userRestController.deleteUser(id);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(result.getBody()).isEqualTo(expectedUserDto);
        verify(userFacade).deleteUser(id);
    }
}