package cz.muni.fi.pa165.xhavlic.user_manager_microservice.service;

import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserLoginDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.MusicStyle;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.User;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.repository.interfaces.MusicStyleRepository;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.repository.interfaces.UserRepository;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.exceptions.UserExceptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static junit.framework.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    public UserRepository userRepository;
    @Mock
    private MusicStyleRepository musicStyleRepository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @InjectMocks
    public UserService userService;

    @Test
    void createUser_userCreationFailed_throwException() {
        List<MusicStyle> styles = List.of(new MusicStyle(1L, "f"));
        var user = new User(1L, "d", "d", "f", styles);

        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());
        when(userRepository.save(any(User.class))).thenThrow(new DataIntegrityViolationException("user"));

        var exception = assertThrows(UserExceptions.UserCreationException.class, () ->
                userService.createUser(user));

        assertThat(exception.getMessage()).contains("Failed to create a user due to a data integrity issue.");
        verify(userRepository).save(user);
    }

    @Test
    void createUser_WithExistingUsername_ThrowsException() {
        User user = new User(null, "admin", "email@example.com", "password", Collections.emptyList());
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(new User()));

        Exception exception = assertThrows(UserExceptions.UserCreationException.class, () ->
                userService.createUser(user));

        assertEquals("Username already exists: admin", exception.getMessage());
    }

    @Test
    void createUser_WithExistingEmail_ThrowsException() {
        User user = new User(null, "newuser", "email@example.com", "password", Collections.emptyList());
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(new User()));

        Exception exception = assertThrows(UserExceptions.UserCreationException.class, () ->
                userService.createUser(user));

        assertEquals("Email already exists: email@example.com", exception.getMessage());
    }

    @Test
    void createUser_WithDataIntegrityIssue_ThrowsException() {
        User user = new User(null, "newuser", "newemail@example.com", "password", Collections.emptyList());
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());
        when(userRepository.save(any(User.class))).thenThrow(DataIntegrityViolationException.class);

        Exception exception = assertThrows(UserExceptions.UserCreationException.class, () ->
                userService.createUser(user));

        assertEquals("Failed to create a user due to a data integrity issue.", exception.getMessage());
    }

    @Test
    void createUser() {
        MusicStyle style = new MusicStyle(1L, "rock");
        User user = new User(1L, "username", "email@example.com", "password", List.of(style));
        String encodedPassword = "encodedPassword";

        when(musicStyleRepository.findByName("rock")).thenReturn(Optional.of(style));
        when(passwordEncoder.encode(user.getPassword())).thenReturn(encodedPassword);
        when(userRepository.save(any(User.class))).thenAnswer(invocation -> {
            User savedUser = invocation.getArgument(0);
            savedUser.setPassword(encodedPassword);
            return savedUser;
        });

        User result = userService.createUser(user);

        verify(musicStyleRepository).findByName("rock");
        verify(passwordEncoder).encode("password");
        verify(userRepository).save(user);
        assertThat(result.getMusicStyles()).containsExactly(style);
        assertThat(result.getPassword()).isEqualTo(encodedPassword);
    }

    @Test
    void updateUser_userInRepository_ReturnsUpdatedUser() {
        MusicStyle oldStyle = new MusicStyle(1L, "rock");
        MusicStyle newStyle = new MusicStyle(2L, "metal");
        User oldUser = new User(1L, "d", "d@example.com", "pass", List.of(oldStyle));
        User newUser = new User(1L, "s", "d@example.com", "pass", List.of(newStyle));

        when(userRepository.findById(1L)).thenReturn(Optional.of(oldUser));
        when(musicStyleRepository.findByName("metal")).thenReturn(Optional.of(newStyle));
        when(userRepository.save(oldUser)).thenReturn(newUser);

        User result = userService.updateUser(1L, newUser);

        verify(userRepository).findById(1L);
        verify(musicStyleRepository).findByName("metal");
        verify(userRepository).save(oldUser);
        assertThat(result.getMusicStyles()).containsExactly(newStyle);
    }

    @Test
    void updateUser_userNotInRepository_ThrowsError() {
        List<MusicStyle> styles = List.of(new MusicStyle(1L, "f"));
        var newUser = new User(1L, "s", "d", "f", styles);

        var id = 1L;

        when(userRepository.findById(id)).thenReturn(Optional.empty());

        var result = assertThrows(UserExceptions.UserNotFoundException.class, () -> userService.updateUser(id, newUser));
        assertThat(result.getMessage()).isEqualTo("User with ID " + id + " not found.");

        verify(userRepository).findById(id);
    }

    @Test
    void validateCredentials_userNotFound_throwsError() {
        var username = "mario";

        var user = new UserLoginDto(username, "***");

        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        var result = assertThrows(UserExceptions.UserNotFoundException.class, () -> userService.validateCredentials(user));
        assertThat(result.getMessage()).isEqualTo("User with username " + username + " not found.");
        verify(userRepository).findByUsername(username);
    }
    @Test
    void deleteUser_userNotExists_throwsError() {
        long id = 1L;
        when(userRepository.getUserById(id)).thenReturn(Optional.empty());

        var resultException = assertThrows(UserExceptions.UserNotFoundException.class, () -> userService.deleteUser(id));

        assertThat(resultException.getMessage()).isEqualTo("User with ID "+id+" not found.");
    }

    @Test
    void deleteUser_userExists_returnUser() {
        long id = 1L;
        List<MusicStyle> styles = List.of(new MusicStyle(1L, "rock"));
        User expectedUser = new User(id, "JohnDoe", "john.doe@example.com", "password123", styles);

        when(userRepository.getUserById(id)).thenReturn(Optional.of(expectedUser));
        when(userRepository.deleteUserById(id)).thenReturn(1);

        var result = userService.deleteUser(id);

        assertThat(result).isEqualTo(expectedUser);
        verify(userRepository).deleteUserById(id);
    }
    @ParameterizedTest
    @CsvSource(value = {
            "heslo,heslo,true",
            "xegwqehq3hqg35q3ggd,dewgwegegx,false"
    })
    void validateCredentials_userFound_PasswordCheck(String passwordLogin, String passwordEncoded, boolean expected) {
        String username = "mario";
        List<MusicStyle> styles = List.of(new MusicStyle(1L, "f"));
        User userFromDb = new User(1L, username, "mail", passwordEncoded, styles);

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(userFromDb));

        when(passwordEncoder.matches(passwordLogin, passwordEncoded)).thenReturn(expected);

        boolean result = userService.validateCredentials(new UserLoginDto(username, passwordLogin));

        assertThat(result).isEqualTo(expected);
        verify(userRepository).findByUsername(username);
        verify(passwordEncoder).matches(passwordLogin, passwordEncoded);
    }

    @Test
    void findAll() {
        List<MusicStyle> styles = List.of(new MusicStyle(1L, "f"));
        var user = new User(1L, "d", "d", "f", styles);
        var users = List.of(user);
        when(userRepository.findAll()).thenReturn(users);
        var result = userService.findAll();

        verify(userRepository).findAll();
        assertThat(result).isEqualTo(users);
    }
}