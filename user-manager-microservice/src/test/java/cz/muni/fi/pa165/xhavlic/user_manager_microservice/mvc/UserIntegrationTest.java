package cz.muni.fi.pa165.xhavlic.user_manager_microservice.mvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserDto;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.api.UserLoginDto;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @AfterAll
    public void dropDb(){
        jdbcTemplate.execute("DROP ALL OBJECTS");
    }

    @Test
    void registerUserWithValidBody() throws Exception {
        UserDto userDto = new UserDto("admin", "admin@new.com", "admin1234", List.of("rock", "metal"));
        String jsonString = objectMapper.writeValueAsString(userDto);

        String responseJson = mockMvc.perform(post("/api/users/register")
                .contentType(MediaType.APPLICATION_JSON).content(jsonString))

                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);



        UserDto user = objectMapper.readValue(responseJson, UserDto.class);

        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo("admin");
        assertThat(user.getEmail()).isEqualTo("admin@new.com");
        assertThat(user.getPassword()).isNotEmpty();
        assertThat(user.getMusicStyles()).isEqualTo(List.of("rock", "metal"));


    }

    @Test
    void registerUserWithInvalidBody() throws Exception {
        String invalidJsonString = "some invalid json";

        mockMvc.perform(post("/api/users/register")
                        .contentType(MediaType.APPLICATION_JSON).content(invalidJsonString))

                .andExpect(status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
    }
    @Test
    void loginUserWithValidCredentials() throws Exception {
        UserLoginDto loginUser = new UserLoginDto("admin", "admin1234");
        String jsonString = objectMapper.writeValueAsString(loginUser);

        mockMvc.perform(post("/api/users/login")
                        .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("User logged in successfully!")));
    }

    @Test
    void loginUserWithInvalidCredentials() throws Exception {
        UserLoginDto loginUser = new UserLoginDto("admin", "wrongpassword");
        String jsonString = objectMapper.writeValueAsString(loginUser);

        mockMvc.perform(post("/api/users/login")
                        .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(containsString("Login failed")));
    }

    @Test
    void deleteUserWithValidId() throws Exception {
        UserDto userDto = new UserDto("admin", "admin@new.com", "admin1234", List.of("rock", "metal"));
        String jsonString = objectMapper.writeValueAsString(userDto);

        mockMvc.perform(post("/api/users/register")
                        .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                        .andExpect(status().isCreated());

        mockMvc.perform(delete("/api/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteUserWithInvalidId() throws Exception {
        mockMvc.perform(delete("/api/users/999")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
