package cz.muni.fi.pa165.xhavlic.user_manager_microservice.repository;

import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.model.User;
import cz.muni.fi.pa165.xhavlic.user_manager_microservice.data.repository.interfaces.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
@ActiveProfiles("test")
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestEntityManager entityManager;

    private User user1;
    private User user2;

    @BeforeEach
    void initDatabaseData() {
        user1 = new User(null, "admin", "joe@doe.com", "admin1234", List.of());
        user2 = new User(null, "Karel", "some@email.com", "easy", List.of());

        user1 = entityManager.persistAndFlush(user1);
        user2 = entityManager.persistAndFlush(user2);
    }

    @AfterEach
    void tearDown() {
        entityManager.clear();
    }

    @Test
    void findByUsername_userFound_returnsUser() {
        Optional<User> optionalUser = userRepository.findByUsername("admin");
        assertThat(optionalUser).isNotEmpty();
        User user = optionalUser.get();
        assertThat(user.getUsername()).isEqualTo("admin");
        assertThat(user.getPassword()).isEqualTo("admin1234");
        assertThat(user.getEmail()).isEqualTo("joe@doe.com");
        assertThat(user.getMusicStyles()).isEmpty();
    }

    @Test
    void findByUsername_userNotFound_returnsEmptyOptional() {
        Optional<User> optionalUser = userRepository.findByUsername("nonexistentUser");
        assertThat(optionalUser).isEmpty();
    }

    @Test
    void deleteUserById_userFound_returnsOne() {
        long id = user1.getId();
        int rowsAffected = userRepository.deleteUserById(id);
        assertThat(rowsAffected).isOne();
    }

    @Test
    void deleteUserById_userNotFound_returnsZero() {
        long id = 999L;
        int rowsAffected = userRepository.deleteUserById(id);
        assertThat(rowsAffected).isZero();
    }

    @Test
    void getUserById_userFound_returnsUser() {
        Optional<User> optionalUser = userRepository.getUserById(user2.getId());
        assertThat(optionalUser).isNotEmpty();
        User user = optionalUser.get();
        assertThat(user.getUsername()).isEqualTo("Karel");
        assertThat(user.getPassword()).isEqualTo("easy");
        assertThat(user.getEmail()).isEqualTo("some@email.com");
        assertThat(user.getMusicStyles()).isEmpty();
    }

    @Test
    void getUserById_userNotFound_returnsOptionalEmpty() {
        long id = 9999L;
        Optional<User> optionalUser = userRepository.getUserById(id);
        assertThat(optionalUser).isEmpty();
    }
    @Test
    void findAllUsers_returnsAllUsers() {
        List<User> users = userRepository.findAll();
        assertThat(users).containsExactlyInAnyOrder(user1, user2);
    }

    @Test
    void saveUser_persistsUserSuccessfully() {
        User newUser = new User(null, "newUser", "new@email.com", "new123", List.of());
        User savedUser = userRepository.save(newUser);
        assertThat(savedUser).isNotNull();
        assertThat(savedUser.getId()).isNotNull();
        assertThat(savedUser.getUsername()).isEqualTo("newUser");
    }

    @Test
    void updateUser_updatesExistingUser() {
        user1.setEmail("updated@email.com");
        User updatedUser = userRepository.save(user1);
        assertThat(updatedUser.getEmail()).isEqualTo("updated@email.com");
    }
}
