package cz.muni.fi.pa165.xhavlic.controler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jakub Uhlarik
 */
@Controller
public class LoginController {


    @GetMapping("/")
    public String index(Model model , @AuthenticationPrincipal OidcUser user){
        model.addAttribute("user", user);

        // put issuer name into a model attribute named "issuerName"
        if (user != null) {
            model.addAttribute("issuerName",
                    "https://oidc.muni.cz/oidc/".equals(user.getIssuer().toString()) ? "MUNI" : "Google");
        }

        return "index";
    }

    @GetMapping("/token")
    public ResponseEntity<OAuth2AccessToken> getBearerToken(@RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client) {
        return ResponseEntity.status(HttpStatus.OK).body(oauth2Client.getAccessToken());
    }
}
