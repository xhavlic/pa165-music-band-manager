package cz.muni.fi.pa165.xhavlic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;

/**
 * @author Jakub Uhlarik
 */
@SpringBootApplication
public class LoginApplication {
    public static void main(String[] args) {
        SpringApplication.run(LoginApplication.class, args);

    }
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeHttpRequests(x -> x
                        // allow anonymous access to listed URLs
                        .requestMatchers("/" ).permitAll()
                        // all other requests must be authenticated
                        .anyRequest().authenticated()
                )
                .oauth2Login(x -> x
                        .successHandler(new SavedRequestAwareAuthenticationSuccessHandler())
                )
                .logout(x -> x
                        // After we log out, redirect to the root page, by default Spring will send you to /login?logout
                        .logoutSuccessUrl("/")
                        // after local logout, do also remote logout at the OIDC Provider too
                        .logoutSuccessHandler(oidcLogoutSuccessHandler())
                )
                .csrf(c -> c
                        //set CSRF token cookie "XSRF-TOKEN" with httpOnly=false that can be read by JavaScript
                        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                        //replace the default XorCsrfTokenRequestAttributeHandler with one that can use value from the cookie
                        .csrfTokenRequestHandler(new CsrfTokenRequestAttributeHandler())
                )
        ;
        return httpSecurity.build();
    }

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    private OidcClientInitiatedLogoutSuccessHandler oidcLogoutSuccessHandler() {
        OidcClientInitiatedLogoutSuccessHandler successHandler =
                new OidcClientInitiatedLogoutSuccessHandler(clientRegistrationRepository);
        successHandler.setPostLogoutRedirectUri("http://localhost:8080/");
        return successHandler;
    }
}