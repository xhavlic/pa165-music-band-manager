package cz.muni.fi.pa165.xhavlic.tour_manager.facade;

import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import cz.muni.fi.pa165.xhavlic.tour_manager.service.MapService;
import cz.muni.fi.pa165.xhavlic.tour_manager.service.TourService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */
@ExtendWith(MockitoExtension.class)
class MapFacadeTest {

    @InjectMocks
    public MapFacade mapFacade;

    @Mock
    public MapService mapService;

    @Mock
    public TourService tourService;

    @Test
    void getMapUrlFromLocation() {
        var location = "Usa";
        var expected = "maps/Usa";
        when(mapService.getMapUrlFromLocation(location)).thenReturn(expected);

        var result = mapFacade.getMapUrlFromLocation(location);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    void getMapUrlFromTourId() {
        var location = "Usa";
        var expected = "maps/Usa/canada";
        var locations = List.of(location, "canada");
        var events = locations.stream().map(loc -> {
            var event = new Event();
            event.setAddress(loc);
            return event;
        }).toList();
        var id = 1;

        when(tourService.getEventsByTourId(id)).thenReturn(events);
        when(mapService.getMapUrlFromListOfLocations(locations)).thenReturn(expected);

        var result = mapFacade.getMapUrlFromTourId(id);
        assertThat(result).isEqualTo(expected);
    }
}