package cz.muni.fi.pa165.xhavlic.tour_manager.mvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.pa165.xhavlic.tour_manager.api.TourDto;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.enums.EventType;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import java.time.LocalDate;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Jakub Uhlarik
 */
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TourIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public TourIntegrationTest() {
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @AfterAll
    public void dropDb(){
        jdbcTemplate.execute("DROP ALL OBJECTS");
    }

    public TourDto save(String name) throws Exception {
        TourDto tourDto = new TourDto();
        tourDto.setName(name);
        String tourJson = objectMapper.writeValueAsString(tourDto);
        MvcResult result = mockMvc.perform(post("/api/tour")
                        .contentType(MediaType.APPLICATION_JSON).content(tourJson))
                .andExpect(status().isCreated())
                .andReturn();

        String tourCreatedJson = result.getResponse().getContentAsString();
        return objectMapper.readValue(tourCreatedJson, TourDto.class);
    }

    void deleteById(Long id) throws Exception {
        mockMvc.perform(delete("/api/tour/" + id))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteById_NotFound_NotFoundStatus() throws Exception {
        var id = 2;
        mockMvc.perform(delete("/api/tour/" + id))
                .andExpect(status().isNotFound());
    }

    @Test
    void saveAndGetById() throws Exception {
        TourDto testTour = save("Test Tour");
        String getByIdJson = mockMvc.perform(get("/api/tour/" + testTour.getId()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        TourDto getByID = objectMapper.readValue(getByIdJson, TourDto.class);
        assertThat(getByID).isEqualToComparingFieldByField(testTour);
        deleteById(testTour.getId());
    }

    @Test
    void getById_NotFound_NotFoundStatus() throws Exception {
        var id = 2;
        mockMvc.perform(get("/api/tour/" + id))
                .andExpect(status().isNotFound());
    }

    @Test
    void getAllTours() throws Exception {
        TourDto testTour = save("Test Tour 1");
        TourDto testTour2 = save("Test Tour 2");
        List<TourDto> expected = List.of(testTour, testTour2);

        String getAllJson = mockMvc.perform(get("/api/tours"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<TourDto> getAll = objectMapper.readValue(getAllJson, new TypeReference<>() {});
        assertThat(getAll.size()).isEqualTo(2);
        assertThat(getAll).usingRecursiveFieldByFieldElementComparatorIgnoringFields("events").containsExactlyInAnyOrderElementsOf(expected);

        deleteById(testTour.getId());
        deleteById(testTour2.getId());
    }

    @Test
    void updateById_found_Updated() throws Exception {
        TourDto testTour = save("Initial Tour");
        String updatedName = "Updated Tour";
        testTour.setName(updatedName);

        String jsonRequest = objectMapper.writeValueAsString(testTour);
        String updatedJson = mockMvc.perform(put("/api/tour/" + testTour.getId())
                        .contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        Tour updated = objectMapper.readValue(updatedJson, Tour.class);
        assertThat(updated.getName()).isEqualTo(updatedName);

        deleteById(testTour.getId());
    }

    @Test
    void updateById_notFount_notFoundStatus() throws Exception {
        var id = 3;
        var updatedName = "updated";
        var set = new Tour(id, updatedName, List.of());
        var jsonRequest = objectMapper.writeValueAsString(set);
        mockMvc.perform(put("/api/tour/" + id).contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
                .andExpect(status().isNotFound());
    }

    @Test
    void addEventsAndGet() throws Exception {
        TourDto savedTour = save("Test Tour for Events");
        Long tourId = savedTour.getId();
        Event newEvent = new Event(LocalDate.now().plusDays(1), "name", "desc", "venue", "adre", EventType.CONCERT, null);
        String expectedEventJson = objectMapper.writeValueAsString(newEvent);
        String savedEventJson = mockMvc.perform(post("/api/tour/" + tourId + "/event")
                        .contentType(MediaType.APPLICATION_JSON).content(expectedEventJson))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Event createdEvent = objectMapper.readValue(savedEventJson, Event.class);
        assertThat(createdEvent.getName()).isEqualTo(newEvent.getName());
        assertThat(createdEvent.getDescription()).isEqualTo(newEvent.getDescription());
        assertThat(createdEvent.getVenueName()).isEqualTo(newEvent.getVenueName());
        assertThat(createdEvent.getAddress()).isEqualTo(newEvent.getAddress());
        assertThat(createdEvent.getType()).isEqualTo(newEvent.getType());

        String allEventsJson = mockMvc.perform(get("/api/tour/" + tourId + "/events")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        List<Event> allEvents = objectMapper.readValue(allEventsJson, new TypeReference<>() {});

        assertThat(allEvents).usingRecursiveFieldByFieldElementComparatorIgnoringFields("id").contains(createdEvent);

        deleteById(tourId);
    }

    @Test
    void addEvent_Tour_NotFound_NotFound_Status() throws Exception {

        var id = 1;
        var expectedEvent = new Event(1, LocalDate.now().plusDays(1), "name", "desc", "venue", "adre", EventType.CONCERT, null);
        var expectedEventJson = objectMapper.writeValueAsString(expectedEvent);
        mockMvc.perform(post("/api/tour/" + id + "/event").contentType(MediaType.APPLICATION_JSON).content(expectedEventJson))
                .andExpect(status().isNotFound())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

    }
    @Test
    void testMultipleEventsToOneTour() throws Exception {
        TourDto savedTour = save("Tour with Multiple Events");
        Event event1 = new Event(LocalDate.now().plusDays(1), "Event1", "Desc1", "Venue1", "Address1", EventType.CONCERT, null);
        Event event2 = new Event(LocalDate.now().plusDays(1), "Event2", "Desc2", "Venue2", "Address2", EventType.FESTIVAL, null);
        mockMvc.perform(post("/api/tour/" + savedTour.getId() + "/event")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(event1)))
                .andExpect(status().isCreated());
        mockMvc.perform(post("/api/tour/" + savedTour.getId() + "/event")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(event2)))
                .andExpect(status().isCreated());

        List<Event> events = objectMapper.readValue(mockMvc.perform(get("/api/tour/" + savedTour.getId() + "/events")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<>() {});
        assertThat(events).hasSize(2);

        deleteById(savedTour.getId());
    }

    @Test
    void testDeleteTourWithEvents() throws Exception {
        TourDto savedTour = save("Tour with Events to Delete");
        Event event = new Event(LocalDate.now().plusDays(1), "Event to Delete", "Desc", "Venue", "Address", EventType.CONCERT, null);
        mockMvc.perform(post("/api/tour/" + savedTour.getId() + "/event")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(event)))
                .andExpect(status().isCreated());

        deleteById(savedTour.getId());
        mockMvc.perform(get("/api/tour/" + savedTour.getId() + "/events"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testUpdatePartialTour() throws Exception {
        TourDto savedTour = save("Initial Name for Partial Update");
        String updatedName = "Updated Name Partially";
        savedTour.setName(updatedName);

        mockMvc.perform(put("/api/tour/" + savedTour.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(savedTour)))
                .andExpect(status().isCreated());

        mockMvc.perform(get("/api/tour/" + savedTour.getId()))
                .andExpect(status().isOk())
                .andDo(result -> {
                    Tour updatedTour = objectMapper.readValue(result.getResponse().getContentAsString(), Tour.class);
                    assertThat(updatedTour.getName()).isEqualTo(updatedName);
                });

        deleteById(savedTour.getId());
    }

    @Test
    void testRetrieveEventsForNonExistingTour() throws Exception {
        mockMvc.perform(get("/api/tour/999/events"))
                .andExpect(status().isNotFound());
    }
}
