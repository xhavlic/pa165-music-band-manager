package cz.muni.fi.pa165.xhavlic.tour_manager.service;

import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces.TourRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */
@ExtendWith(MockitoExtension.class)
class TourServiceTest {

    @InjectMocks
    private TourService tourService;

    @Mock
    public TourRepository tourRepository;


    @Test
    void getTourById_tourPresented_returnsTour() {
        var id = 1;
        var expectedTour = new Tour(1, "tour", new ArrayList<>());
        when(tourRepository.getTourById(id)).thenReturn(Optional.of(expectedTour));

        var result = tourService.getTourById(id);

        assertThat(result).isEqualTo(expectedTour);
        verify(tourRepository).getTourById(id);
    }
    @Test
    void getTourById_tourNotPresented_throwError() {
        var id = 1;
        when(tourRepository.getTourById(id)).thenReturn(Optional.empty());

        var resultException = assertThrows(ResponseStatusException.class,()->tourService.getTourById(id));

        assertThat(resultException.getReason()).isEqualTo("Tour with id: "+id+" not found");
        assertThat(resultException.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(tourRepository).getTourById(id);
    }

    @Test
    void createTour_SuccessfullyAdd_returnsTour() {
        var expectedTour = new Tour(1, "tour", new ArrayList<>());
        when(tourRepository.save(expectedTour)).thenReturn(expectedTour);

        var result = tourService.createTour(expectedTour);

        assertThat(result).isEqualTo(expectedTour);
        verify(tourRepository).save(expectedTour);

    }


    @Test
    void deleteTour_tourNotExists_throwsError() {
        var id = 1;
        when(tourRepository.getTourById(id)).thenReturn(Optional.empty());


        var resultException = assertThrows(ResponseStatusException.class,()->tourService.deleteTour(id));

        assertThat(resultException.getReason()).isEqualTo("Tour with id: "+id+" not found");
        assertThat(resultException.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void deleteTour_tourExists_returnTour() {
        var id = 1;
        var expectedTour = new Tour(1, "tour", new ArrayList<>());
        when(tourRepository.getTourById(id)).thenReturn(Optional.of(expectedTour));
        when(tourRepository.deleteTourById(id)).thenReturn(1);

        var result = tourService.deleteTour(id);

        assertThat(result).isEqualTo(expectedTour);
        verify(tourRepository).deleteTourById(id);
    }


    @Test
    void getAllTours_toursInRepo_toursReturned() {
        var expectedTours = List.of(new Tour(1, "tour", new ArrayList<>()));
        when(tourRepository.findAll()).thenReturn(expectedTours);

        var result = tourService.getAllTours();

        assertThat(result).isEqualTo(expectedTours);
        verify(tourRepository).findAll();
    }
}