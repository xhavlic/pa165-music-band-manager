package cz.muni.fi.pa165.xhavlic.tour_manager.repository;

import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces.TourRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Jakub Uhlarik
 */
@DataJpaTest
@ActiveProfiles("test")
class TourRepositoryUnitTest {

    @Autowired
    private TourRepository tourRepository;

    @Autowired
    private TestEntityManager entityManager;

    @AfterEach
    void cleanUp() {
        entityManager.clear();
    }

    @Test
    void getTourById() {
        Tour tour = new Tour("Summer Tour", List.of());
        Tour saved = entityManager.persistFlushFind(tour);
        Tour found = tourRepository.findById(saved.getId()).orElse(null);
        assertThat(found).isEqualTo(saved);
    }

    @Test
    void getTourById_NotFound_OptionalEmpty() {
        assertThat(tourRepository.findById(999L).isEmpty()).isTrue();
    }

    @Test
    void findAll() {
        Tour tour = new Tour("Spring Tour", List.of());
        entityManager.persistFlushFind(tour);
        List<Tour> allTours = tourRepository.findAll();
        assertThat(allTours).usingRecursiveFieldByFieldElementComparatorIgnoringFields("events")
                .containsExactly(tour);
    }

    @Test
    void deleteTourById() {
        Tour tour = new Tour("Winter Tour", List.of());
        Tour saved = entityManager.persistFlushFind(tour);
        entityManager.remove(saved);
        entityManager.flush();
        assertThat(tourRepository.existsById(saved.getId())).isFalse();
    }

    @Test
    void deleteTourById_NotFound() {
        tourRepository.deleteById(999L);
        entityManager.flush();
        boolean existsAfterDelete = tourRepository.existsById(999L);
        assertThat(existsAfterDelete).isFalse();
    }

    @Test
    void updateNameById() {
        Tour tour = new Tour("Autumn Tour", List.of());
        Tour saved = entityManager.persistFlushFind(tour);
        String updatedName = "Updated Autumn Tour";
        tourRepository.updateNameById(updatedName, saved.getId());
        entityManager.flush();
        Tour updated = tourRepository.findById(saved.getId()).orElse(null);
        assertThat(updated).isNotNull();
        assertThat(updated.getName()).isEqualTo(updatedName);
    }

    @Test
    void updateNameById_NotFound() {
        assertThat(tourRepository.updateNameById("Non-existent Tour", 999L)).isEqualTo(0);
    }
}