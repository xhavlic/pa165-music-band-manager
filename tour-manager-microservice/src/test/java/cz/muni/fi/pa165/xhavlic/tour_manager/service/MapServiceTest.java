package cz.muni.fi.pa165.xhavlic.tour_manager.service;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Jakub Uhlarik
 */
class MapServiceTest {

    private final MapService mapService = new MapService();

    @Test
    void getMapUrlFromLocation_CorrectLocation_LocationReturned() {
        var mapUrl = "https://maps.google.com/?q=";
        var location = "NY";
        var expected = mapUrl+location;
        var result = mapService.getMapUrlFromLocation(location);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    void getMapUrlFromListOfLocations_CorrectLocations_LocationReturned() {
        var mapUrl = "https://maps.google.com/maps/dir/";
        var locations = List.of( "NY","LA");
        var expected = mapUrl+"NY/LA/";
        var result = mapService.getMapUrlFromListOfLocations(locations);
        assertThat(result).isEqualTo(expected);
    }
}