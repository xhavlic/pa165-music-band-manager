package cz.muni.fi.pa165.xhavlic.tour_manager.controller;

import cz.muni.fi.pa165.xhavlic.tour_manager.api.TourDto;
import cz.muni.fi.pa165.xhavlic.tour_manager.facade.TourFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */
@ExtendWith(MockitoExtension.class)
class TourRestControllerTest {

    @InjectMocks
    public TourRestController tourRestController;

    @Mock
    public TourFacade tourFacade;

    @Test
    void getTourById() {
        var expectedTourDto = new TourDto();
        expectedTourDto.setId(1);
        var id = 1L;
        when(tourFacade.getTourById(id)).thenReturn(expectedTourDto);

        var result = tourRestController.getTourById(id);
        verify(tourFacade).getTourById(id);
        assertThat(result.getBody()).isEqualTo(expectedTourDto);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void addNewTour() {
        var expectedTourDto = new TourDto();
        expectedTourDto.setId(1);
        when(tourFacade.addNewTour(expectedTourDto)).thenReturn(expectedTourDto);

        var result = tourRestController.addNewTour(expectedTourDto);
        verify(tourFacade).addNewTour(expectedTourDto);
        assertThat(result.getBody()).isEqualTo(expectedTourDto);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void getAllTours() {
        var expectedTourDto = new TourDto();
        expectedTourDto.setId(1);
        var expectedTourDtos = List.of(expectedTourDto);
        when(tourFacade.getAllTours()).thenReturn(expectedTourDtos);

        var result = tourRestController.getAllTours();
        verify(tourFacade).getAllTours();
        assertThat(result.getBody()).isEqualTo(expectedTourDtos);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void updateTour() {
        var expectedTourDto = new TourDto();
        long id = 1;
        expectedTourDto.setId(1);
        when(tourFacade.updateTour(expectedTourDto, id)).thenReturn(expectedTourDto);

        var result = tourRestController.updateTour(expectedTourDto, id);
        verify(tourFacade).updateTour(expectedTourDto, id);
        assertThat(result.getBody()).isEqualTo(expectedTourDto);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void deleteTour() {
        var expectedTourDto = new TourDto();
        expectedTourDto.setId(1);
        var id = 1L;
        when(tourFacade.deleteTour(id)).thenReturn(expectedTourDto);

        var result = tourRestController.deleteTour(id);
        verify(tourFacade).deleteTour(id);

        assertThat(result.getBody()).isEqualTo(expectedTourDto);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}