package cz.muni.fi.pa165.xhavlic.tour_manager.controller;

import cz.muni.fi.pa165.xhavlic.tour_manager.facade.MapFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */
@ExtendWith(MockitoExtension.class)
class MapRestControllerTest {

    @Mock
    public MapFacade mapFacade;

    @InjectMocks
    public MapRestController mapRestController;

    @Test
    void getMapUrlFromLocation() {

        var location = "usa";
        var expected = "expected_usa";
        when(mapFacade.getMapUrlFromLocation(location)).thenReturn(expected);

        var result = mapRestController.getMapUrlFromLocation(location);
        assertThat(result).isEqualTo(expected);
        verify(mapFacade).getMapUrlFromLocation(location);
    }

    @Test
    void getMapUrlFromTourId() {
        var id = 1;
        var expected = "expected_usa";
        when(mapFacade.getMapUrlFromTourId(id)).thenReturn(expected);

        var result = mapRestController.getMapUrlFromTourId(id);
        assertThat(result).isEqualTo(expected);
        verify(mapFacade).getMapUrlFromTourId(id);
    }
}