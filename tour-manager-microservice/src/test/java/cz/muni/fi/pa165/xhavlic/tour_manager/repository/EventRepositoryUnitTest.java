package cz.muni.fi.pa165.xhavlic.tour_manager.repository;

import cz.muni.fi.pa165.xhavlic.tour_manager.data.enums.EventType;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces.EventRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Jakub Uhlarik
 */
@DataJpaTest
@ActiveProfiles("test")
class EventRepositoryUnitTest {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private TestEntityManager entityManager;

    @AfterEach
    void cleanUp() {
        entityManager.clear();
    }

    @Test
    void saveAndGetEventById_EqualAsSaved() {
        Event event = new Event(LocalDate.now(), "name", "desc", "venue", "address", EventType.MEDIA_EVENT, null);
        Event saved = entityManager.persistFlushFind(event);
        Event found = eventRepository.findById(saved.getId()).orElse(null);
        assertThat(found).isEqualTo(saved);
    }

    @Test
    void getEventById_NotInDbNotFound() {
        assertThat(eventRepository.findById(999L).isEmpty()).isTrue();
    }

    @Test
    void saveAndGetEventsByTourId_Saved_Found() {
        Tour tour = new Tour("Tour Name", null);
        Tour savedTour = entityManager.persistFlushFind(tour);
        Event event = new Event(LocalDate.now(), "name", "desc", "venue", "address", EventType.MEDIA_EVENT, savedTour);
        entityManager.persistFlushFind(event);
        List<Event> events = eventRepository.getEventsByTourId(savedTour.getId());
        assertThat(events).containsExactly(event);
    }

    @Test
    void saveAndGetEventById_2Saved_2Found() {
        Tour tour = new Tour("Tour Name", null);
        Tour savedTour = entityManager.persistFlushFind(tour);
        Event event1 = new Event(LocalDate.now(), "name", "desc", "venue", "address", EventType.MEDIA_EVENT, savedTour);
        Event event2 = new Event(LocalDate.now(), "name", "desc", "venue", "address", EventType.MEDIA_EVENT, savedTour);
        entityManager.persist(event1);
        entityManager.persist(event2);
        entityManager.flush();

        List<Event> events = eventRepository.getEventsByTourId(savedTour.getId());
        assertThat(events).hasSize(2).containsExactlyInAnyOrder(event1, event2);
    }

    @Test
    @Transactional
    void deleteEvent() {
        Event event = new Event(LocalDate.now(), "name", "desc", "venue", "address", EventType.MEDIA_EVENT, null);
        Event saved = entityManager.persistFlushFind(event);
        eventRepository.delete(saved);
        assertThat(eventRepository.findById(saved.getId()).isEmpty()).isTrue();
    }

    @Test
    void updateEventDetails() {
        Event event = new Event(LocalDate.now(), "name", "desc", "venue", "address", EventType.MEDIA_EVENT, null);
        Event saved = entityManager.persistFlushFind(event);
        saved.setName("Updated Name");
        eventRepository.save(saved);

        Event updated = eventRepository.findById(saved.getId()).orElse(null);
        assertThat(updated).isNotNull();
        assertThat(updated.getName()).isEqualTo("Updated Name");
    }

    @Test
    void eventsWithNoTour() {
        Event event = new Event(LocalDate.now(), "name", "desc", "venue", "address", EventType.MEDIA_EVENT, null);
        entityManager.persistFlushFind(event);

        List<Event> events = eventRepository.getEventsByTourId(999L);
        assertThat(events).isEmpty();
    }

    @Test
    void multipleEventsDifferentTours() {
        Tour tour1 = new Tour("Tour One", null);
        Tour tour2 = new Tour("Tour Two", null);
        entityManager.persist(tour1);
        entityManager.persist(tour2);
        Event event1 = new Event(LocalDate.now(), "Event One", "desc", "venue", "address", EventType.MEDIA_EVENT, tour1);
        Event event2 = new Event(LocalDate.now(), "Event Two", "desc", "venue", "address", EventType.MEDIA_EVENT, tour2);
        entityManager.persist(event1);
        entityManager.persist(event2);
        entityManager.flush();

        assertThat(eventRepository.getEventsByTourId(tour1.getId())).containsExactly(event1);
        assertThat(eventRepository.getEventsByTourId(tour2.getId())).containsExactly(event2);
    }
}
