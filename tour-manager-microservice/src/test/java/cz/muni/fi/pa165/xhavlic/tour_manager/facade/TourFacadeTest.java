package cz.muni.fi.pa165.xhavlic.tour_manager.facade;

import cz.muni.fi.pa165.xhavlic.tour_manager.api.TourDto;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import cz.muni.fi.pa165.xhavlic.tour_manager.mapper.TourMapper;
import cz.muni.fi.pa165.xhavlic.tour_manager.service.TourService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */
@ExtendWith(MockitoExtension.class)
class TourFacadeTest {

    @InjectMocks
    public TourFacade tourFacade;

    @Mock
    public TourMapper tourMapper;

    @Mock
    public TourService tourService;

    @Test
    void getTourById() {
        var expectedTourDto = new TourDto();
        expectedTourDto.setId(1);
        var expectedTour = new Tour(1, "list", List.of());
        var id = 1;

        when(tourMapper.mapToDto(expectedTour)).thenReturn(expectedTourDto);
        when(tourService.getTourById(id)).thenReturn(expectedTour);

        var result = tourFacade.getTourById(id);
        assertThat(result).isEqualTo(expectedTourDto);

        verify(tourService).getTourById(id);
        verify(tourMapper).mapToDto(expectedTour);
    }

    @Test
    void addNewTour() {
        var expectedTourDto = new TourDto();
        expectedTourDto.setId(1);
        var expectedTour = new Tour(1, "list", List.of());
        var id = 1;

        when(tourMapper.mapToDto(expectedTour)).thenReturn(expectedTourDto);
        when(tourMapper.mapFromDto(expectedTourDto)).thenReturn(expectedTour);
        when(tourService.createTour(expectedTour)).thenReturn(expectedTour);

        var result = tourFacade.addNewTour(expectedTourDto);
        assertThat(result).isEqualTo(expectedTourDto);

        verify(tourService).createTour(expectedTour);
        verify(tourMapper).mapFromDto(expectedTourDto);
    }

    @Test
    void updateTour() {
        var expectedTourDto = new TourDto();
        expectedTourDto.setId(1);
        var expectedTour = new Tour(1, "list", List.of());
        var id = 1;

        when(tourMapper.mapToDto(expectedTour)).thenReturn(expectedTourDto);
        when(tourMapper.mapFromDto(expectedTourDto)).thenReturn(expectedTour);
        when(tourService.updateTour(id, expectedTour)).thenReturn(expectedTour);

        var result = tourFacade.updateTour(expectedTourDto, id);
        assertThat(result).isEqualTo(expectedTourDto);

        verify(tourService).updateTour(1, expectedTour);
        verify(tourMapper).mapFromDto(expectedTourDto);
    }

    @Test
    void deleteTour() {
        var expectedTourDto = new TourDto();
        expectedTourDto.setId(1);
        var expectedTour = new Tour(1, "list", List.of());
        var id = 1;

        when(tourMapper.mapToDto(expectedTour)).thenReturn(expectedTourDto);
        when(tourService.deleteTour(id)).thenReturn(expectedTour);

        var result = tourFacade.deleteTour(id);
        assertThat(result).isEqualTo(expectedTourDto);

        verify(tourService).deleteTour(id);
        verify(tourMapper).mapToDto(expectedTour);
    }

    @Test
    void getAllTours() {
        var expectedTourDto = new TourDto();
        expectedTourDto.setId(1);
        var expectedTourDtos = List.of(expectedTourDto);
        var expectedTour = new Tour(1, "list", List.of());
        var expectedTours = List.of(expectedTour);

        when(tourMapper.mapToDto(expectedTour)).thenReturn(expectedTourDto);
        when(tourService.getAllTours()).thenReturn(expectedTours);

        var result = tourFacade.getAllTours();
        assertThat(result).isEqualTo(expectedTourDtos);

        verify(tourService).getAllTours();
        verify(tourMapper).mapToDto(expectedTour);
    }
}