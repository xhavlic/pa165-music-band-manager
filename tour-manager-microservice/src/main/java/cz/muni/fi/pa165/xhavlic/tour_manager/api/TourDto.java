package cz.muni.fi.pa165.xhavlic.tour_manager.api;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class TourDto {
    long id;

    @Schema(example = "Eras Tour", requiredMode = Schema.RequiredMode.REQUIRED)
    String name;

    @ArraySchema(schema = @Schema(implementation = EventDto.class, requiredMode = Schema.RequiredMode.NOT_REQUIRED))
    List<Event> events = new ArrayList<>();
}
