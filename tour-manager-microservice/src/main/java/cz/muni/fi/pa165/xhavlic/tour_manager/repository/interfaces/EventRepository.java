package cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces;

import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EventRepository extends JpaRepository<Event, Long> {
    Optional<Event> getEventById(long id);

    List<Event> getEventsByTourId(long tourId);
}
