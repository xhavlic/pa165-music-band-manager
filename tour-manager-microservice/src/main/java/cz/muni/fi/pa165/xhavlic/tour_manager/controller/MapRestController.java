package cz.muni.fi.pa165.xhavlic.tour_manager.controller;

import cz.muni.fi.pa165.xhavlic.tour_manager.facade.MapFacade;
import cz.muni.fi.pa165.xhavlic.tour_manager.oauth.TourAuthentication;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/map")
public class MapRestController {
    private final MapFacade mapFacade;

    @Autowired
    public MapRestController(MapFacade mapFacade) {
        this.mapFacade = mapFacade;
    }

    @Operation(summary = "Get a map URL from given location", description = "Creates a URL for google map for a given location")
    @SecurityRequirement(name= TourAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned map url",
            content = {@Content(mediaType = "text/plain")}),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content)}
    )
    @GetMapping("/{location}")
    public String getMapUrlFromLocation(@PathVariable String location) {
        return mapFacade.getMapUrlFromLocation(location);
    }

    @Operation(summary = "Get a waypoint map for Tour", description = "Creates a URL for google map for a tour, creating a waypoint for each Event")
    @SecurityRequirement(name= TourAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned map url",
                    content = {@Content(mediaType = "text/plain")}),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content)}
    )
    @GetMapping("/tour/{tourId}")
    public String getMapUrlFromTourId(@PathVariable long tourId) {
        return mapFacade.getMapUrlFromTourId(tourId);
    }

}
