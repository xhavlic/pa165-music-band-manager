package cz.muni.fi.pa165.xhavlic.tour_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TourManagerApp {

    public static void main(String[] args) {
        SpringApplication.run(TourManagerApp.class, args);
    }

}
