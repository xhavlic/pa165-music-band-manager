package cz.muni.fi.pa165.xhavlic.tour_manager.facade;

import cz.muni.fi.pa165.xhavlic.tour_manager.api.EventDto;
import cz.muni.fi.pa165.xhavlic.tour_manager.api.TourDto;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import cz.muni.fi.pa165.xhavlic.tour_manager.mapper.EventMapper;
import cz.muni.fi.pa165.xhavlic.tour_manager.mapper.TourMapper;
import cz.muni.fi.pa165.xhavlic.tour_manager.service.EventService;
import cz.muni.fi.pa165.xhavlic.tour_manager.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class TourFacade {
    private final TourService tourService;
    private final TourMapper tourMapper;

    private final EventMapper eventMapper;

    private final EventService eventService;

    @Autowired
    public TourFacade(TourService tourService, TourMapper tourMapper, EventMapper eventMapper, EventService eventService) {
        this.tourService = tourService;
        this.tourMapper = tourMapper;
        this.eventMapper = eventMapper;
        this.eventService = eventService;
    }

    public TourDto getTourById(long id) {
        return tourMapper.mapToDto(tourService.getTourById(id));
    }

    public TourDto addNewTour(TourDto tour) {
        return tourMapper.mapToDto(tourService.createTour(tourMapper.mapFromDto(tour)));
    }

    public TourDto updateTour(TourDto tour, long id) {
        return tourMapper.mapToDto(tourService.updateTour(id, tourMapper.mapFromDto(tour)));
    }

    public TourDto deleteTour(long id) {
        return tourMapper.mapToDto(tourService.deleteTour(id));
    }

    public Collection<TourDto> getAllTours() {
        return tourService.getAllTours().stream()
                .map(tourMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public Collection<EventDto> getEventsByTourId(Long id) {
        if (tourService.getTourById(id) == null)
            return null;
        return tourService.getEventsByTourId(id).stream()
                .map(eventMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public EventDto addEventToTour(Long tourId, EventDto eventDto) {
        Event event = eventMapper.mapFromDto(eventDto);
        event = eventService.addEventToTour(tourId, event);
        return eventMapper.mapToDto(event);
    }
}
