package cz.muni.fi.pa165.xhavlic.tour_manager.data.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.enums.EventType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.proxy.HibernateProxy;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Event implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private LocalDate date;
    private String name;
    private String description;
    private String venueName;
    private String address;
    private EventType type;
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "tour_id")
    private Tour tour;

    public Event(LocalDate date, String name, String description, String venueName, String address, EventType type, Tour tour) {
        this.date = date;
        this.name = name;
        this.description = description;
        this.venueName = venueName;
        this.address = address;
        this.type = type;
        this.tour = tour;
    }
    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        Event event = (Event) o;
        return Objects.equals(getId(), event.getId()) &&
                Objects.equals(getAddress(),event.getAddress()) &&
                Objects.equals(getName(),event.getName()) &&
                Objects.equals(getDescription(),event.getDescription()) &&
                Objects.equals(getVenueName(),event.getVenueName()) &&
                Objects.equals(getDate(),event.getDate()) &&
                Objects.equals(getType(),event.getType()) &&
                Objects.equals(getTour(),event.getTour())  ;
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}
