package cz.muni.fi.pa165.xhavlic.tour_manager.controller;


import cz.muni.fi.pa165.xhavlic.tour_manager.api.EventDto;
import cz.muni.fi.pa165.xhavlic.tour_manager.api.TourDto;
import cz.muni.fi.pa165.xhavlic.tour_manager.facade.TourFacade;
import cz.muni.fi.pa165.xhavlic.tour_manager.oauth.TourAuthentication;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api")
public class TourRestController {
    private final TourFacade tourFacade;

    @Autowired
    public TourRestController(TourFacade tourFacade) {
        this.tourFacade = tourFacade;
    }

    @Operation(summary = "Get a tour by id", description = "Return Tour based per the id")
    @SecurityRequirement(name = TourAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the tour",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TourDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id given", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Tour not found", content = @Content)})
    @GetMapping("/tour/{id}")
    public ResponseEntity<TourDto> getTourById(@PathVariable Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(tourFacade.getTourById(id));
    }

    @Operation(summary = "Add new tour", description = "Add new tour and return added tour")
    @SecurityRequirement(name = TourAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Added the tour",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TourDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid body given", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content())})

    @PostMapping("/tour")
    public ResponseEntity<TourDto> addNewTour(@RequestBody TourDto tour) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(tourFacade.addNewTour(tour));
    }

    @Operation(summary = "Get all tours", description = "Get a list of all tours")
    @SecurityRequirement(name = TourAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the tours",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = TourDto.class)))}),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content())})
    @GetMapping("/tours")
    public ResponseEntity<Collection<TourDto>> getAllTours() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(tourFacade.getAllTours());
    }

    @Operation(summary = "Update a tour", description = "Update tour with id to tour in body")
    @SecurityRequirement(name = TourAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Updated tour",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TourDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or body supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Tour not found", content = @Content)})
    @PutMapping("/tour/{id}")
    public ResponseEntity<TourDto> updateTour(@RequestBody TourDto tour, @PathVariable Long id) {

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(tourFacade.updateTour(tour, id));
    }

    @Operation(summary = "Delete a tour", description = "Delete a tour with a given id")
    @SecurityRequirement(name = TourAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Successfully deleted tour", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = TourDto.class))
            }),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_1", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Tour not found")

    })
    @DeleteMapping("/tour/{id}")
    public ResponseEntity<TourDto> deleteTour(@PathVariable Long id) {
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .contentType(MediaType.APPLICATION_JSON)
                .body(tourFacade.deleteTour(id));
    }

    @Operation(summary = "Get events for a tour", description = "Get events for a given tour id")
    @SecurityRequirement(name = TourAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the events",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = EventDto.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or body supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_read", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Tour not found", content = @Content)
    })
    @GetMapping("/tour/{id}/events")
    public ResponseEntity<Collection<EventDto>> getEvents(@PathVariable Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(tourFacade.getEventsByTourId(id));
    }

    @Operation(summary = "Add event to tour", description = "Creates and adds an event to a tour based on tour id")
    @SecurityRequirement(name = TourAuthentication.SECURITY_SCHEME_BEARER)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Added the event",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = EventDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or body supplied", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized - access token not provided or not valid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "Forbidden - access token does not have scope test_write", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Tour not found", content = @Content)
    })
    @PostMapping("/tour/{tourId}/event")
    public ResponseEntity<EventDto> addEventToTour(@PathVariable Long tourId, @RequestBody EventDto event) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(tourFacade.addEventToTour(tourId, event));
    }
}
