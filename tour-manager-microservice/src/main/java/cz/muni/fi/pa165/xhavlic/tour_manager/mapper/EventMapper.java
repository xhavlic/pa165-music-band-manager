package cz.muni.fi.pa165.xhavlic.tour_manager.mapper;

import cz.muni.fi.pa165.xhavlic.tour_manager.api.EventDto;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", uses = {TourMapper.class})
public interface EventMapper {
    EventDto mapToDto(Event event);

    Event mapFromDto(EventDto eventDto);

    List<EventDto> mapToList(List<Event> events);
}