package cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces;

import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface TourRepository extends JpaRepository<Tour, Long> {

    Optional<Tour> getTourById(long id);

    List<Tour> findAll();

    int deleteTourById(long id);


    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update Tour t set t.name = ?1 where t.id = ?2")
    int updateNameById(String name, long id);
}
