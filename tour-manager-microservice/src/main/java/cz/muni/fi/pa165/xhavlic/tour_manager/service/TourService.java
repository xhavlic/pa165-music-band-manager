package cz.muni.fi.pa165.xhavlic.tour_manager.service;

import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces.EventRepository;
import cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces.TourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.List;

@Service
public class TourService {
    private final TourRepository tourRepository;
    private final EventRepository eventRepository;


    @Autowired
    public TourService(TourRepository tourRepository, EventRepository eventRepository) {
        this.tourRepository = tourRepository;
        this.eventRepository = eventRepository;
    }

    public Event getEventById(long id) {
        return eventRepository.getEventById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Event with id: " + id + " not found")
        );
    }

    public List<Event> getEventsByTourId(long tourId) {
        return eventRepository.getEventsByTourId(tourId);
    }

    public Tour getTourById(long id) {
        return tourRepository.getTourById(id)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, "Tour with id: " + id + " not found")
                );

    }

    @Transactional
    public Tour createTour(Tour tour) {
        return tourRepository.save(tour);
    }

    @Transactional
    public Tour deleteTour(long id) {
        Tour tour = tourRepository.getTourById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tour with id: " + id + " not found")
        );
        int deletedRows = tourRepository.deleteTourById(id);
        if (deletedRows == 0) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Tour with id: " + id + " could not be deleted");
        }
        return tour;
    }

    public Collection<Tour> getAllTours() {
        return tourRepository.findAll();
    }

    @Transactional
    public Tour updateTour(long id, Tour tour) {
        var updated = tourRepository.updateNameById(tour.getName(), id);
        if (updated == 0) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Tour with id: " + id + " not found");
        }
        tour.setName(tour.getName());
        return tour;
    }
}
