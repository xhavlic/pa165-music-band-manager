package cz.muni.fi.pa165.xhavlic.tour_manager.config;

import com.github.javafaker.Faker;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.enums.EventType;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces.EventRepository;
import cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces.TourRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class DatabaseSeederConfig {

    private static final Faker faker = new Faker();
    private static final int numberOfTours = faker.number().numberBetween(15, 25);

    @Bean
    @Transactional
    public CommandLineRunner databaseSeeder(EventRepository eventRepository, TourRepository tourRepository) {
        return args -> {
            String seedData = System.getProperty("seed.data", "false");
            String clearDb = System.getProperty("clear.db", "false");
            System.out.println("Seed data property: " + seedData);
            System.out.println("Clear DB property: " + clearDb);

            // Optionally clear the database
            if (Boolean.parseBoolean(clearDb)) {
                System.out.println("Clearing the database...");
                eventRepository.deleteAll();
                tourRepository.deleteAll();
            }
            if (Boolean.parseBoolean(seedData)) {
                // Generate and save Tours and Events
                List<Tour> tours = generateTours();
                tourRepository.saveAll(tours);

                tours.forEach(tour -> {
                    List<Event> events = generateEventsForTour(tour);
                    eventRepository.saveAll(events);
                });

                System.out.println("Database has been seeded");
            }
        };
    }

    private List<Tour> generateTours() {
        List<Tour> tours = new ArrayList<>();
        for (int i = 0; i < numberOfTours; i++) {
            Tour tour = new Tour();
            tour.setName(faker.rockBand().name() + " Tour");
            tours.add(tour);
        }
        return tours;
    }

    private List<Event> generateEventsForTour(Tour tour) {
        List<Event> events = new ArrayList<>();
        for (int i = 0; i < faker.number().numberBetween(1, 5); i++) {
            Event event = new Event();
            event.setDate(LocalDate.now().plusDays(faker.number().numberBetween(1, 365)));
            event.setName(faker.music().instrument() + " Festival");
            event.setDescription("Experience the ultimate " + event.getName());
            event.setVenueName(faker.company().name() + " Arena");
            event.setAddress(faker.address().fullAddress());
            event.setType(EventType.CONCERT);
            event.setTour(tour);
            events.add(event);
        }
        return events;
    }
}