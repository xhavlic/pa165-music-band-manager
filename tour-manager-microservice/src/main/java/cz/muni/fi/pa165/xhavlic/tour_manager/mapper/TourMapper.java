package cz.muni.fi.pa165.xhavlic.tour_manager.mapper;

import cz.muni.fi.pa165.xhavlic.tour_manager.api.TourDto;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {EventMapper.class})
public interface TourMapper {
    TourDto mapToDto(Tour tour);

    Tour mapFromDto(TourDto tourDto);

    List<TourDto> mapToList(List<Tour> tours);
}
