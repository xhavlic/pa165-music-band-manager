package cz.muni.fi.pa165.xhavlic.tour_manager.service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MapService {

    public String getMapUrlFromLocation(String location) {
        return "https://maps.google.com/?q=" + location;
    }

    public String getMapUrlFromListOfLocations(List<String> locations) {
        StringBuilder sb = new StringBuilder();
        for (String location : locations) {
            sb.append(location);
            sb.append("/");
        }
        return "https://maps.google.com/maps/dir/" + sb.toString();
    }

}
