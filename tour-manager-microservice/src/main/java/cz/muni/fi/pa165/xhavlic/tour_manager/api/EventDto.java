package cz.muni.fi.pa165.xhavlic.tour_manager.api;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.enums.EventType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * DTO for {@link cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event}
 */
@Value
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class EventDto implements Serializable {
    long id;
    @Schema(example = "2024-04-23", requiredMode = Schema.RequiredMode.REQUIRED)
    LocalDate date;
    @Schema(example = "Derniere Event", requiredMode = Schema.RequiredMode.REQUIRED)
    String name;
    @Schema(example = "Last event ever", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    String description;
    @Schema(example = "Tokio Dome", requiredMode = Schema.RequiredMode.REQUIRED)
    String venueName;
    @Schema(example = "Tokio" ,requiredMode = Schema.RequiredMode.REQUIRED)
    String address;
    @Schema(example = "CONCERT" ,requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    EventType type;
    TourDto tour;
}