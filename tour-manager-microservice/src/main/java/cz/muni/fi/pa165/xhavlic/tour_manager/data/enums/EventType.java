package cz.muni.fi.pa165.xhavlic.tour_manager.data.enums;

public enum EventType {
    CONCERT,
    FESTIVAL,
    MEDIA_EVENT,
    OTHER
}
