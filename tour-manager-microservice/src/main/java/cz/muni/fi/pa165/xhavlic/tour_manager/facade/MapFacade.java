package cz.muni.fi.pa165.xhavlic.tour_manager.facade;

import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import cz.muni.fi.pa165.xhavlic.tour_manager.service.MapService;
import cz.muni.fi.pa165.xhavlic.tour_manager.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MapFacade {
    private final MapService mapService;
    private final TourService tourService;

    @Autowired
    public MapFacade(MapService mapService, TourService tourService) {
        this.mapService = mapService;
        this.tourService = tourService;
    }

    public String getMapUrlFromLocation(String location) {
        return mapService.getMapUrlFromLocation(location);
    }

    public String getMapUrlFromTourId(long tourId) {
        List<String> locations = new java.util.ArrayList<>(List.of());
        for (Event event : tourService.getEventsByTourId(tourId)) {
            if (event.getAddress() != null) {
                locations.add(event.getAddress());
            }
        }
        return mapService.getMapUrlFromListOfLocations(locations);
    }
}
