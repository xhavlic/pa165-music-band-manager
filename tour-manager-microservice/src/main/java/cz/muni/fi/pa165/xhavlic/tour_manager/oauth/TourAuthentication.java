package cz.muni.fi.pa165.xhavlic.tour_manager.oauth;

import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.stereotype.Component;

/**
 * @author Jakub Uhlarik
 */
@Component
public class TourAuthentication {
    public static final String SECURITY_SCHEME_BEARER = "Bearer";

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.GET, "/api/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/api/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.PUT, "/api/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.DELETE, "/api/**").hasAuthority("SCOPE_test_1")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()));
        return http.build();
    }

    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> {
            openApi.getComponents()
                    .addSecuritySchemes(SECURITY_SCHEME_BEARER,
                            new SecurityScheme()
                                    .type(SecurityScheme.Type.HTTP)
                                    .scheme("bearer")
                                    .description("provide a valid access token")
                    )
            ;
        };
    }
}
