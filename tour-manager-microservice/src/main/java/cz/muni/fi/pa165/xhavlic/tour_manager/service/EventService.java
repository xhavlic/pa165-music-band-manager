package cz.muni.fi.pa165.xhavlic.tour_manager.service;

import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Event;
import cz.muni.fi.pa165.xhavlic.tour_manager.data.model.Tour;
import cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces.EventRepository;
import cz.muni.fi.pa165.xhavlic.tour_manager.repository.interfaces.TourRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import java.time.LocalDate;

@Service
public class EventService {
    EventRepository eventRepository;

    TourRepository tourRepository;

    @Autowired
    public EventService(EventRepository eventRepository, TourRepository tourRepository) {
        this.eventRepository = eventRepository;
        this.tourRepository = tourRepository;
    }

    @Transactional
    @Modifying
    public Event addEventToTour(Long tourId, Event event) {
        if (event.getDate() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Event date cannot be null");
        }
        if (event.getDate().isBefore(LocalDate.now())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Event date cannot be in the past.");
        }

        Tour tour = tourRepository.getTourById(tourId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,"Tour with id: " + tourId + " not found")
        );
        event.setTour(tour);
        return eventRepository.save(event);
    }
}
